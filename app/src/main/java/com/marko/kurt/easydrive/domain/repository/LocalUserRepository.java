package com.marko.kurt.easydrive.domain.repository;

import com.marko.kurt.easydrive.domain.User;

import io.reactivex.Single;

/**
 * Created by user1 on 9.12.2017..
 */

public interface LocalUserRepository {

    Single<User> getLocalUser();

}
