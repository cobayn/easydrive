package com.marko.kurt.easydrive.presentation.presenter.event;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.exception.ErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.events.DislikeEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.LikeEventUseCase;
import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcastReceiverView;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by nsoft on 13/12/2017.
 */

public class BroadcastReceiverPresenterImpl extends BasePresenter<LikeBroadcastReceiverView> implements BroadcastReceiverPresenter{

    private LikeEventUseCase likeEventUsecase;
    private DislikeEventUseCase dislikeEventUseCase;

    public BroadcastReceiverPresenterImpl(LikeBroadcastReceiverView view, LikeEventUseCase like, DislikeEventUseCase dislike) {
        super(view);
        this.likeEventUsecase = like;
        this.dislikeEventUseCase = dislike;
    }

    @Override
    public void likeEvent(short eventGroupId) {
        likeEventUsecase.execute(new ServerResponseDisposable(), new short[]{eventGroupId, 1});
    }

    @Override
    public void dislikeEvent(short eventGroupId) {
        dislikeEventUseCase.execute(new ServerResponseDisposable(), new short[]{eventGroupId, 1});
    }

    private class ServerResponseDisposable extends DeafultSingleObserver<ServerLoginReponse>{

        @Override
        public void onSuccess(ServerLoginReponse serverLoginReponse) {
            getNullableView().showSuccesfullMessage(serverLoginReponse.getMessage());
        }

        @Override
        public void onError(Throwable e) {
            ErrorBundle errorBundle = new DefaultErrorBundle((Exception) e);
            getNullableView().showError(ErrorMessageFactory.create(errorBundle.getException()));
        }
    }

}
