package com.marko.kurt.easydrive.presentation.presenter.event;

/**
 * Created by nsoft on 13/12/2017.
 */

public interface BroadcastReceiverPresenter {

    public void likeEvent(short eventGroupId);

    public void dislikeEvent(short eventGroupId);

}
