package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by user1 on 28.8.2017..
 */

public interface ActivationCodePresenter extends ScopedPresenter {

    void validateCode(String code);

}
