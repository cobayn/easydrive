package com.marko.kurt.easydrive.data.net;

import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;
import com.marko.kurt.easydrive.data.entity.UserEntity;
import com.marko.kurt.easydrive.data.util.AppUtils;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by nsoft on 08/04/2017.
 */

public interface LoginApi {

    String CODE_STATUS = "";

    String CODE_ACTIVATE = "";

    String CLIENT_LOGIN = "";

    String ANONYMUS_LOGIN = "";

    String CLIENT_REGISTRATION = "";

    String REPAIR_PASSWORD = "";

    String SYSTEM_LOGIN = "";

    @POST(REPAIR_PASSWORD)
    public Observable<ServerLoginResponseEntity> repairPassword(@Header(AppUtils.HEADER_REQUEST_CONTENT_TYPE) String content, @Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activationCode, @Header(AppUtils.HEADER_DEVICE_ID) String device_id,
                                      @Field("password") String password);
    @POST(CLIENT_REGISTRATION)
    public Single<Response<ServerLoginResponseEntity>> registerUser(@Header(AppUtils.HEADER_REQUEST_CONTENT_TYPE) String content, @Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activationCode, @Header(AppUtils.HEADER_DEVICE_ID) String device_id,
                                                             @Body UserEntity user);

    @POST(CODE_ACTIVATE)
    public Single<Response<ServerLoginResponseEntity>> activateCode(@Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activationCode, @Header(AppUtils.HEADER_DEVICE_ID) String device_id);

    @POST(CLIENT_LOGIN)
    public Single<Response<Object>> loginUser(@Header(AppUtils.HEADER_REQUEST_CONTENT_TYPE) String content_type, @Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activationCode,
                                                                @Header(AppUtils.HEADER_DEVICE_ID) String device_id, @Body UserEntity userEntity);

    @POST(ANONYMUS_LOGIN)
    public Single<Response<Object>> loginAnonymusUser(@Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activationCode,
                                                               @Body UserEntity userEntity);

    @GET(CODE_STATUS)
    public Single<Response<ServerLoginResponseEntity>> checkStatusOfCode(@Header(AppUtils.HEADER_AUTHENTICATION_CODE) String activation_code,
                                                                   @Header(AppUtils.HEADER_DEVICE_ID) String device_id);

}
