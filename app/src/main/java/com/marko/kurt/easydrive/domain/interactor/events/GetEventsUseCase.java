package com.marko.kurt.easydrive.domain.interactor.events;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.domain.interactor.DefaultObservableUseCase;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by user1 on 12.8.2017..
 */

public class GetEventsUseCase extends DefaultObservableUseCase<Event, double[]> {

    private final EventRepository eventRepository;

    public GetEventsUseCase(CompositeDisposable compositeDisposable, EventRepository eventRepository){
        super(compositeDisposable);
        this.eventRepository = eventRepository;
    }

    @Override
    public Observable<Event> buildSingleUseCase(double[] param) {
        return eventRepository.getEvents(param[0], param[1]);
    }
}
