package com.marko.kurt.easydrive.domain.exception;

/**
 * Created by user1 on 5.11.2017..
 */

public interface ErrorBundle {

    Exception getException();

    String getErrorMessage();

}
