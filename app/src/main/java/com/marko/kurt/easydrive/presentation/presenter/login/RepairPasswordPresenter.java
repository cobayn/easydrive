package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by nsoft on 08/05/2017.
 */

public interface RepairPasswordPresenter extends ScopedPresenter {

    public void repairPassword(String password);

}
