package com.marko.kurt.easydrive.data.util;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by nsoft on 31/08/2017.
 */

public class TelephonIdGenerator {

    private Context mContext;

    public TelephonIdGenerator(Context context){
        this.mContext = context;
    }

    public String newId() {
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

}
