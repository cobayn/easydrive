package com.marko.kurt.easydrive.presentation.presenter.event;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.exception.ErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.DefaultObserver;
import com.marko.kurt.easydrive.domain.interactor.events.CreateEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.GetEventsUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.GetLocalUserUsecase;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.mapper.EventViewModeMapper;
import com.marko.kurt.easydrive.presentation.mapper.UserViewModelMapper;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.MapView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by nsoft on 31/08/2017.
 */

public class MapPresenterImpl extends BasePresenter<MapView> implements MapPresenter {

    private GetEventsUseCase getEventsUseCase;
    private CreateEventUseCase createEventUseCase;
    private GetLocalUserUsecase getLocalUserUsecase;
    private EventViewModeMapper mapper;
    private UserViewModelMapper userMapper;
    private List<Event> eventsList = new ArrayList<>();

    @Inject
    public MapPresenterImpl(MapView view, GetEventsUseCase useCase,
                            CreateEventUseCase createusecase, EventViewModeMapper mapper, UserViewModelMapper userMapper,
                            GetLocalUserUsecase localUserUsecase) {
        super(view);
        this.getEventsUseCase = useCase;
        this.createEventUseCase = createusecase;
        this.mapper = mapper;
        this.getLocalUserUsecase = localUserUsecase;
        this.userMapper = userMapper;
    }

    @Override
    public void getEvents(double lat, double lng) {
        getEventsUseCase.execute(new EventDisposable(), new double[]{lat, lng});
    }

    @Override
    public void createEvent(double lat, double lng, short eventType, String description) {
        createEventUseCase.execute(lat, lng, eventType, description);
    }

    /**
     * if user is admin or moderator, then he can add events and can move cursor on map
     * if user is not admin or moderator, he has centered position
     * @param type determine what kind of user is logged in
     */

    @Override
    public void validateUser() {
        getLocalUserUsecase.getLocalUser(new UserDisposable());
    }

    /**
     * delete code from shared preferences
     */
    @Override
    public void deactivateCode() {
        createEventUseCase.deactivateUserCode();
    }

    private class EventDisposable extends DefaultObserver<Event> {
        @Override
        public void onNext(Event events) {
            eventsList.add(events);
        }

        @Override
        public void onComplete() {
            super.onComplete();
            getNullableView().showEvents(mapper.mapCollectionsToViewModel(eventsList));
            if (eventsList.size() != 0){
                getNullableView().showNotification();
            }
        }

        @Override
        public void onError(Throwable e) {
            ErrorBundle errorBundle = new DefaultErrorBundle((Exception) e);
            getNullableView().showError(ErrorMessageFactory.create(errorBundle.getException()));
        }
    }

    private class UserDisposable extends DeafultSingleObserver<User>{

        @Override
        public void onError(Throwable e) {
            super.onError(e);
        }

        @Override
        public void onSuccess(User user) {

            if (user.getUserType() < 3){
                getNullableView().alowUserToSeeRestOfMap();
            } else {
                getNullableView().setCenteredUser();
            }
            getNullableView().setUserInformactionsToNavigationView(userMapper.transform(user));

        }
    }

}
