package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.RegistrationUserUseCase;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.RegistrationView;

/**
 * Created by nsoft on 21/04/2017.
 */

public class RegistrationPresenterImpl extends BasePresenter<RegistrationView> implements RegistrationPresenter {

    private SingleUseCase mRegistrationUseCase;

    public RegistrationPresenterImpl(RegistrationView view, RegistrationUserUseCase loginUsercase) {
        super(view);
        this.mRegistrationUseCase = loginUsercase;
    }

    @Override
    public void registerUser(String username, String password, String repassword, String email) {

        if (username.isEmpty()){
            return;
        }

        if (password.isEmpty()) {
            getNullableView().setPasswordError();
            return;
        }

        if (email.isEmpty()) {
            getNullableView().setEmailError();
            return;
        }

        if (!password.equals(repassword)) {
            getNullableView().setMismatchPasswordError();
            return;
        }

        if (getNullableView() != null)
            getNullableView().showLoading();

        mRegistrationUseCase.execute(new RegistrationObservable(), new String[]{email, username, password});
    }

    @Override
    public void openAnonymusMode(){

    }

    private class RegistrationObservable extends DeafultSingleObserver<ServerLoginReponse>{

        @Override
        public void onSuccess(ServerLoginReponse serverLoginReponse) {
            getNullableView().hideLoading();
            getNullableView().onSuccess(serverLoginReponse.getMessage());
        }

        @Override
        public void onError(Throwable e) {
            getNullableView().hideLoading();
            String errorMessage =  ErrorMessageFactory.create(new DefaultErrorBundle((Exception) e).getException());
            getNullableView().showError(errorMessage);
        }
    }

}
