package com.marko.kurt.easydrive.presentation.util;

/**
 * Created by user1 on 31.10.2017..
 */

public interface OnControlSnackbarListener {

    public void showErrorSnackbar(String message);
    public void showSuccessfullSnackbar(String message);

}
