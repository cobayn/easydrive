package com.marko.kurt.easydrive.presentation.view;

import com.marko.kurt.easydrive.presentation.model.EventModel;
import com.marko.kurt.easydrive.presentation.model.UserModel;

import java.util.List;

/**
 * Created by nsoft on 21/04/2017.
 */

public interface MapView extends LoadDataView{

    void showEvents(List<EventModel> eventModelList);

    void setCenteredUser();

    void alowUserToSeeRestOfMap();

    void setUserInformactionsToNavigationView(UserModel userModel);

    void showNotification();

}
