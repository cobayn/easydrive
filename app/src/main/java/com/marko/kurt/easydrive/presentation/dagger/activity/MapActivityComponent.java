package com.marko.kurt.easydrive.presentation.dagger.activity;

import com.marko.kurt.easydrive.presentation.dagger.activity.module.MapActivityModule;
import com.marko.kurt.easydrive.presentation.view.activity.MapActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 2.9.2017..
 */
@Subcomponent(modules = MapActivityModule.class)
public interface MapActivityComponent  extends AndroidInjector<MapActivity>{

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MapActivity>{}

}
