package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by nsoft on 08/04/2017.
 */

public interface LoginPresenter extends ScopedPresenter {

    public void validateInputs(String username, String password, boolean rememberMe);

    public void anonymusUser();

}
