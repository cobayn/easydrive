package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.user.LoginUsecase;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.LoginView;

/**
 * Created by nsoft on 08/04/2017.
 */

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    private LoginUsecase loginUsecase;

    public LoginPresenterImpl(LoginView view, LoginUsecase usecase) {
        super(view);
        this.loginUsecase = usecase;
    }

    @Override
    public void validateInputs(String username, String password, boolean remember_me) {

        if (username.isEmpty()){
            getNullableView().setUsernameError();
            return;
        }

        if (password.isEmpty()){
            getNullableView().setPasswordError();
            return;
        }

        if (getNullableView() != null)
            getNullableView().showLoading();


        loginUsecase.executeLoginUser(new LoginObservable(false), new String[]{username, password}, remember_me);
    }

    @Override
    public void anonymusUser() {

        if (getNullableView() != null)
            getNullableView().showAnonymusDetails();

        loginUsecase.execute(new LoginObservable(true), null);

    }

    private class LoginObservable extends DeafultSingleObserver<User>{

        private boolean mAnonymus;

        LoginObservable(boolean anonymus){
            this.mAnonymus = anonymus;
        }

        @Override
        public void onSuccess(User serverLoginReponse) {
            if (!mAnonymus)
                getNullableView().hideLoading();
            else
                getNullableView().hideAnonymusDetails();
            getNullableView().onSuccess();
        }

        @Override
        public void onError(Throwable e) {
            if (!mAnonymus)
                getNullableView().hideLoading();
            else
                getNullableView().hideAnonymusDetails();
            String errorMessage =  ErrorMessageFactory.create(new DefaultErrorBundle((Exception) e).getException());
            getNullableView().showError(errorMessage);

        }
    }

}
