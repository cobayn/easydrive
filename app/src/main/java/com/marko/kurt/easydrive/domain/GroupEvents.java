package com.marko.kurt.easydrive.domain;

/**
 * Created by kurt on 16/11/2017.
 */

public class GroupEvents {

    private short id, eventType;
    private String description;
    private double lat, lng;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
