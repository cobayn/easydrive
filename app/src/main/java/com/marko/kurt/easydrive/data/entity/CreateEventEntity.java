package com.marko.kurt.easydrive.data.entity;

/**
 * Created by user1 on 25.11.2017..
 */

public class CreateEventEntity {

    private double lat, lng;
    private short eventType;
    private String description;

    public CreateEventEntity() {}

    public CreateEventEntity(double lat, double lng, short eventType, String description){
        this.lat = lat;
        this.lng = lng;
        this.eventType = eventType;
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
