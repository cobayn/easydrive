package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by nsoft on 06/09/2017.
 */

public class RegistrationUserUseCase extends DefaultSingleUseCase<ServerLoginReponse, String[]>{

    private UserRepository mUserRepository;

    @Inject
    public RegistrationUserUseCase(CompositeDisposable compositeDisposable, UserRepository userRepository) {
        super(compositeDisposable);
        this.mUserRepository = userRepository;
    }

    @Override
    public Single<ServerLoginReponse> buildSingleUseCase(String[] param) {
        return mUserRepository.registerUser(param[0], param[1], param[2]);
    }
}
