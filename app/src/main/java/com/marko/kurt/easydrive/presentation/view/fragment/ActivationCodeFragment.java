package com.marko.kurt.easydrive.presentation.view.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.ActivationCodePresenter;
import com.marko.kurt.easydrive.presentation.view.ActivationCodeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ActivationCodeFragment extends BaseLoginFragment implements ActivationCodeView {

    @BindView(R.id.code_actionvation_edittext)
    EditText activation_code;
    @BindView(R.id.activate_progressbar)
    ProgressBar pBar;
    @BindView(R.id.activate_button)
    Button activation_button;
    @BindView(R.id.lock_activation_cordinatorLayout)
    ConstraintLayout unlock_constraintlayout;
    @BindView(R.id.code_activation_inputlayout)
    TextInputLayout activationCodeLayout;

    @Inject
    ActivationCodePresenter presenter;

    @Override
    protected int setLayout() {
        return R.layout.fragment_activation_code;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showLoading() {
        setEnabled(false);
        pBar.setVisibility(View.VISIBLE);
        activation_button.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoading() {
        setEnabled(true);
        pBar.setVisibility(View.GONE);
        activation_button.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.lock_activation_cordinatorLayout)
    public void onLockButtonClickListener(){
        presenter.validateCode(activation_code.getText().toString());
    }

    @Override
    public void onSuccess(String key) {
        activityUtils.removeFragmentFromActivity(fragmentManager, this);
        activityUtils.addFragmentToActivity(fragmentManager, new LoginFragment(), R.id.content_login);
    }

    @Override
    public void onActivationCodeError() {
        activationCodeLayout.setError("Unesite pravi kod");
    }

    private void setEnabled(boolean enabled){
        activation_code.setEnabled(enabled);
        unlock_constraintlayout.setEnabled(enabled);
    }

}
