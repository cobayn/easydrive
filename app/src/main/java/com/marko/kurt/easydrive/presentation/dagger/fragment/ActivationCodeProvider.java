package com.marko.kurt.easydrive.presentation.dagger.fragment;

import android.support.v4.app.Fragment;

import com.marko.kurt.easydrive.presentation.view.fragment.ActivationCodeFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.LoginFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.RegistrationFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.RepairFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * Created by user1 on 2.9.2017..
 */
@Module
public abstract class ActivationCodeProvider {

    @Binds
    @IntoMap
    @FragmentKey(LoginFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideLoginFactory(LoginFragmentComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(ActivationCodeFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideActivationCodeFactory(ActivationCodeComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(RegistrationFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideRegistrationFactory(RegistrationFragmentComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(RepairFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> provideRepairFactory(RepairFragmentComponent.Builder builder);

}
