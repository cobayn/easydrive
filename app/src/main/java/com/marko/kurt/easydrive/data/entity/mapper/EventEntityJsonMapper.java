package com.marko.kurt.easydrive.data.entity.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marko.kurt.easydrive.data.entity.EventEntity;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by nsoft on 02/07/2017.
 */

public class EventEntityJsonMapper {

    private final Gson gson;

    public EventEntityJsonMapper(){
        this.gson = new Gson();
    }

    public EventEntity transformEventEntity(String eventJsonResponse){
        Type eventEntityType = new TypeToken<EventEntity>() {}.getType();
        return this.gson.fromJson(eventJsonResponse, eventEntityType);
    }

    public List<EventEntity> transformEventEntityCollection(String eventListJsonResponse){
        Type listOfEventEntityType = new TypeToken<EventEntity>() {}.getType();
        return this.gson.fromJson(eventListJsonResponse, listOfEventEntityType);
    }

}
