package com.marko.kurt.easydrive.presentation.dagger.broadcastReceiver;

import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcaseReceiver;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent(modules = LikeBroadcastReceiverModule.class)
public interface LikeBroadcastReceiverComponent extends AndroidInjector<LikeBroadcaseReceiver> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LikeBroadcaseReceiver>{}

}
