package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.domain.interactor.user.LoginUsecase;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.RepairFragmentView;

/**
 * Created by nsoft on 08/05/2017.
 */

public class RepairPasswordPresenterImpl extends BasePresenter<RepairFragmentView> implements RepairPasswordPresenter {

    LoginUsecase loginUsecase;

    public RepairPasswordPresenterImpl(RepairFragmentView view) {
        super(view);
    }

    @Override
    public void repairPassword(String password) {
        if (password.isEmpty()){
            getNullableView().showError("Please type some data");
        }
    }
}
