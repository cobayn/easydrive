package com.marko.kurt.easydrive.presentation.dagger.activity;

import com.marko.kurt.easydrive.presentation.dagger.activity.module.LoginActivityModule;
import com.marko.kurt.easydrive.presentation.dagger.fragment.ActivationCodeProvider;
import com.marko.kurt.easydrive.presentation.view.activity.LoginActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 2.9.2017..
 */
@Subcomponent(modules = {LoginActivityModule.class, ActivationCodeProvider.class})
public interface LoginActivityComponent extends AndroidInjector<LoginActivity>{

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoginActivity>{}

}
