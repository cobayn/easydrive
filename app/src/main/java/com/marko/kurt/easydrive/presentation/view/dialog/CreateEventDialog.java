package com.marko.kurt.easydrive.presentation.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.util.CreateEventListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by user1 on 1.12.2017..
 */

public class CreateEventDialog extends DialogFragment {

    private CreateEventListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getContext(), R.style.FullscreenTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_create_event, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CreateEventListener){
            mListener = (CreateEventListener) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.radar_imageview)
    public void onRadarClickListener(){
        this.dismiss();
        mListener.createEventListener((short)1);
    }

    @OnClick(R.id.police_imageview)
    public void onPoliceClickListener(){
        this.dismiss();
        mListener.createEventListener((short)2);
    }

    @OnClick(R.id.zatvorena_cesta_imageview)
    public void onCloseRoadClickListener(){
        this.dismiss();
        mListener.createEventListener((short)3);
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }
}
