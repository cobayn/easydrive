package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.entity.GroupEventsEntity;
import com.marko.kurt.easydrive.domain.GroupEvents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kurt on 17/11/2017.
 */

public class GroupEventsEntityDataMapper {

    public GroupEvents transform(GroupEventsEntity groupsEventsEntity){

        GroupEvents groupEvents = null;
        if (groupsEventsEntity != null){
            groupEvents = new GroupEvents();
            groupEvents.setId(groupsEventsEntity.getId());
            groupEvents.setDescription(groupsEventsEntity.getDescription());
            groupEvents.setEventType(groupsEventsEntity.getEventType());
            groupEvents.setLat(groupsEventsEntity.getLat());
            groupEvents.setLng(groupsEventsEntity.getLng());
        }
        return groupEvents;

    }

    public List<GroupEvents> transformCollections(List<GroupEventsEntity> listData){

        List<GroupEvents> eventsList = new ArrayList<>();
        for (GroupEventsEntity groupEventsEntity : listData){

            GroupEvents groupEvents = transform(groupEventsEntity);
            if (groupEvents != null)
                eventsList.add(groupEvents);

        }

        return eventsList;

    }

}
