package com.marko.kurt.easydrive.domain.interactor;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by kurt on 14/11/2017.
 */

public class DeafultSingleObserver<T> extends DisposableSingleObserver<T> {
    @Override
    public void onSuccess(T t) {

    }

    @Override
    public void onError(Throwable e) {

    }
}
