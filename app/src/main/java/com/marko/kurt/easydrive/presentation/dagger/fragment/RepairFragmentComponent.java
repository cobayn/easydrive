package com.marko.kurt.easydrive.presentation.dagger.fragment;

import com.marko.kurt.easydrive.presentation.dagger.fragment.module.RepairFragmentModule;
import com.marko.kurt.easydrive.presentation.view.fragment.RepairFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 21.9.2017..
 */

@Subcomponent(modules = RepairFragmentModule.class)
public interface RepairFragmentComponent extends AndroidInjector<RepairFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RepairFragment>{}

}
