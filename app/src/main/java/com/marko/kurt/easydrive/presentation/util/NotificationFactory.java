package com.marko.kurt.easydrive.presentation.util;

import android.app.Notification;
import android.app.PendingIntent;

/**
 * Created by user1 on 2.12.2017..
 */

public interface NotificationFactory {

    Notification createNewNotification(String warning_type, boolean isLiked, boolean isDisiked, boolean isMuted);

}
