package com.marko.kurt.easydrive.data.util.connectivity;

/**
 * Created by user1 on 19.8.2017..
 */

public interface ConnectivityManagerWrapper {

    boolean isConnectedToNetwork();

    NetworkData getNetworkData();

}
