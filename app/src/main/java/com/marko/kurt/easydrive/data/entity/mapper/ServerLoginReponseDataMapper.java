package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;
import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.ServerLoginReponse;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by nsoft on 07/09/2017.
 */

public class ServerLoginReponseDataMapper {

    private Serializer mSerializer;

    public ServerLoginReponseDataMapper(Serializer serializer){
        this.mSerializer = serializer;
    }

    public ServerLoginReponse transform(ServerLoginResponseEntity responseEntity){
        ServerLoginReponse loginReponse = null;
        if (responseEntity != null){
            loginReponse = new ServerLoginReponse(responseEntity.getCode(), responseEntity.getMessage(), responseEntity.getType(), responseEntity.isUserLoggedIn());
        }
        return loginReponse;
    }

    public ServerLoginReponse transformResponse(Response<ServerLoginResponseEntity> responseEntity) throws NetworkConnectionException {
        if (responseEntity.isSuccessful()){
            return transform(responseEntity.body());
        } else {
            try {
                ServerLoginResponseEntity se = mSerializer.deserialize(responseEntity.errorBody().string(), ServerLoginResponseEntity.class);//new Gson().fromJson(responseEntity.errorBody().string(), ServerLoginResponseEntity.class);
                throw new NetworkConnectionException(new Throwable(se.getMessage()));
            } catch (IOException | NullPointerException exception){
                throw new NetworkConnectionException(exception);
            }
        }
    }

}