package com.marko.kurt.easydrive.presentation.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.marko.kurt.easydrive.presentation.dagger.fragment.DaggerFragment;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

import butterknife.ButterKnife;

/**
 * Created by nsoft on 08/04/2017.
 */

public abstract class BaseFragment extends DaggerFragment{

    protected abstract int setLayout();
    protected abstract void init(View view);
    protected abstract ScopedPresenter getPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().start();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(setLayout(), container, false);
        ButterKnife.bind(this, view);
        init(view);
        return view;
    }

    protected void toastMessage(String message){
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void onPreDestroy(){
        getPresenter().destroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().activate();
    }

    @Override
    public void onPause() {
        getPresenter().deactivate();
        super.onPause();
    }
}
