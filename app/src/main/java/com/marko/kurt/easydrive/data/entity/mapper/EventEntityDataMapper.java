package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.db.dao.EventDao;
import com.marko.kurt.easydrive.data.entity.EventEntity;
import com.marko.kurt.easydrive.data.entity.GroupEventsEntity;
import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;
import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by nsoft on 02/07/2017.
 */

public class EventEntityDataMapper {

    private Serializer mSerializer;

    public EventEntityDataMapper(Serializer serializer){
        this.mSerializer = serializer;
    }

    public Event transform(EventEntity eventEntity){
        Event event = null;
        if (eventEntity != null){
            event = new Event();
            event.setId(eventEntity.getId());
            event.setLng(eventEntity.getLng());
            event.setLat(eventEntity.getLat());
            event.setDescritpion(eventEntity.getDescription());
            event.setEventType(eventEntity.getEventType());
        }
        return event;
    }

    public List<Event> transform(List<EventEntity> eventEntityCollection){
        List<Event> eventList = new ArrayList<>();
        for (EventEntity eventEntity : eventEntityCollection){
            Event event = transform(eventEntity);
            if (event != null){
                eventList.add(event);
            }
        }
        return eventList;
    }

    public List<EventEntity> transformFromObjectList(Response<Object> objectList) throws NetworkConnectionException{

        if (objectList.isSuccessful()) {

            List<EventEntity> entityList = new ArrayList<>();

            try {

                JSONObject responseList = new JSONObject(mSerializer.serializeObject(objectList.body())).getJSONObject("body");
                JSONArray jsonArray = responseList.getJSONArray("eventGroups");
                entityList = mSerializer.deserializeList(jsonArray.toString(), EventEntity[].class);
                for (EventEntity eventEntity : entityList) {

                    for (GroupEventsEntity groupEventsEntity : eventEntity.getGroupEvents()) {

                        if (groupEventsEntity.getLng() != 0 && groupEventsEntity.getLat() != 0) {

                            eventEntity.setLat(groupEventsEntity.getLat());
                            eventEntity.setLng(groupEventsEntity.getLng());
                            eventEntity.setDescription(groupEventsEntity.getDescription());
                            eventEntity.setEventType(groupEventsEntity.getEventType());
                            break;

                        }

                    }

                }
            } catch (JSONException exception) {
                exception.printStackTrace();
                throw new NetworkConnectionException(exception);
            }

            return entityList;
        } else {
            try {
                ServerLoginResponseEntity se = mSerializer.deserialize(objectList.errorBody().string(), ServerLoginResponseEntity.class);
                throw new NetworkConnectionException(new Throwable(se.getMessage()));
            } catch (IOException | NullPointerException exception){
                throw new NetworkConnectionException(exception);
            }
        }

    }

}
