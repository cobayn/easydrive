package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by nsoft on 21/04/2017.
 */

public interface RegistrationPresenter extends ScopedPresenter{

    public void registerUser(String username, String password, String repassword, String email);

    public void openAnonymusMode();

}
