package com.marko.kurt.easydrive.domain.interactor.type;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by user1 on 12.8.2017..
 */

public interface SingleUseCase<P, R> {

    void execute(DisposableSingleObserver<P> observer, R param);

}
