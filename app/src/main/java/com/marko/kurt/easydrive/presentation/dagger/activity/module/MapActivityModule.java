package com.marko.kurt.easydrive.presentation.dagger.activity.module;

import com.marko.kurt.easydrive.domain.interactor.events.CreateEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.GetEventsUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.GetLocalUserUsecase;
import com.marko.kurt.easydrive.presentation.mapper.EventViewModeMapper;
import com.marko.kurt.easydrive.presentation.mapper.UserViewModelMapper;
import com.marko.kurt.easydrive.presentation.presenter.event.MapPresenter;
import com.marko.kurt.easydrive.presentation.presenter.event.MapPresenterImpl;
import com.marko.kurt.easydrive.presentation.util.NotificationFactory;
import com.marko.kurt.easydrive.presentation.util.NotificationFactoryImpl;
import com.marko.kurt.easydrive.presentation.util.Notifications;
import com.marko.kurt.easydrive.presentation.util.NotificationsImpl;
import com.marko.kurt.easydrive.presentation.view.MapView;
import com.marko.kurt.easydrive.presentation.view.activity.MapActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 2.9.2017..
 */
@Module
public class MapActivityModule extends ActivityModule<MapActivity>{

    @Provides
    MapView provideMapView(MapActivity mapActivity){
        return mapActivity;
    }

    @Provides
    MapPresenter provideMapPresenter(MapActivity mapActivity,
                                     GetEventsUseCase eventsUseCase, CreateEventUseCase createEventUsecase, EventViewModeMapper mapper,
                                     UserViewModelMapper userViewModelMapper, GetLocalUserUsecase userLocalUsercase){
        return new MapPresenterImpl(mapActivity, eventsUseCase, createEventUsecase, mapper, userViewModelMapper, userLocalUsercase);
    }

    @Provides
    Notifications provideNotification(MapActivity mapActivity){
        return new NotificationsImpl(mapActivity);
    }

    @Provides
    NotificationFactory provideNotificationFactory(MapActivity mapActivity){
        return new NotificationFactoryImpl(mapActivity);
    }

}
