package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.repository.LocalUserRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user1 on 9.12.2017..
 */

public class GetLocalUserUsecase {

    private LocalUserRepository mLocalUserRepository;
    private CompositeDisposable mCompositeDisposable;

    public GetLocalUserUsecase(CompositeDisposable compositeDisposable, LocalUserRepository localUserRepository){
        mCompositeDisposable = compositeDisposable;
        this.mLocalUserRepository = localUserRepository;
    }

    public void getLocalUser(DisposableSingleObserver<User> observer){
        mCompositeDisposable.add(mLocalUserRepository.getLocalUser()
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(observer));
    }

}
