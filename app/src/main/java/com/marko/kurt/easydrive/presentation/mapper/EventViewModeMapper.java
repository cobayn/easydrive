package com.marko.kurt.easydrive.presentation.mapper;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.presentation.model.EventModel;

import java.util.List;

/**
 * Created by user1 on 18.8.2017..
 */

public interface EventViewModeMapper {

    EventModel mapEventToViewModel(Event event);

    List<EventModel> mapCollectionsToViewModel(List<Event> eventList);

}
