package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.type.LoginSingleUsecase;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nsoft on 02/07/2017.
 */

public class LoginUsecase extends DefaultSingleUseCase<User, Void> implements
        LoginSingleUsecase<User, Void>{

    private final UserRepository userRepository;

    @Inject
    public LoginUsecase(CompositeDisposable compositeDisposable, UserRepository repository){
        super(compositeDisposable);
        this.userRepository = repository;
    }

    @Override
    public void executeLoginUser(DisposableSingleObserver<User> observer, String[] credentials, boolean remember_me) {
        addSingle(userRepository.loginUser(credentials[0], credentials[1], remember_me)
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(observer));
    }

    @Override
    public Single<User> buildSingleUseCase(Void param) {
        return userRepository.loginAnonymusUser();
    }




















    /*@Override
    public Single<ServerLoginReponse> buildSingleUseCase(String[] param) {
        return userRepository.loginUser(param[0], param[1]);
    }

    public void buildAnonymusUseCase(DisposableSingleObserver<ServerLoginReponse> observer){
        Single<ServerLoginReponse> observable = userRepository.loginAnonymusUser()
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        addSingle(observable.subscribeWith(observer));
    }

    @Override
    public void execute(DisposableSingleObserver<ServerLoginReponse> observer, Void param) {

    }

    @Override
    public void executeLoginUser(String[] credentials, boolean remember_me) {

    }*/
}
