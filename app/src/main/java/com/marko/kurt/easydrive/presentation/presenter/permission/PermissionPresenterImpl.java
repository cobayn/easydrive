package com.marko.kurt.easydrive.presentation.presenter.permission;

import android.content.pm.PackageManager;

import com.marko.kurt.easydrive.data.PermissionAction;
import com.marko.kurt.easydrive.presentation.model.PermissionModel;

/**
 * Created by nsoft on 21/08/2017.
 */

public class PermissionPresenterImpl implements PermissionPresenter{

    private PermissionAction permissionAction;
    private PermissionCallbacks permissionCallbacks;

    public PermissionPresenterImpl(PermissionAction permissionAction, PermissionCallbacks permissionCallbacks){
        this.permissionAction = permissionAction;
        this.permissionCallbacks = permissionCallbacks;
    }

    private void checkAndRequestPermission(PermissionModel permissionModel){
        if (permissionAction.hasSelfPermission(permissionModel.getPermission())) {
            permissionCallbacks.permissionAccepted(permissionModel.getCode());
        } else {
            if (permissionAction.shouldShowRequestPermissionRationale(permissionModel.getPermission())){
                permissionCallbacks.showRationale(permissionModel.getCode());
            } else {
                requestPermission(permissionModel);
            }

        }
    }

    private void checkAndRequestPermissions(PermissionModel[] permissionModels){
        if (permissionAction.hasSelfPermission(permissionModels[0].getPermission()) && permissionAction.hasSelfPermission(permissionModels[1].getPermission()))
            permissionCallbacks.permissionAccepted(permissionModels[0].getCode());
        else {
            if (permissionAction.shouldShowRequestPermissionRationale(permissionModels[0].getPermission()) || permissionAction.shouldShowRequestPermissionRationale(permissionModels[1].getPermission())){
                permissionCallbacks.showRationale(permissionModels[0].getCode());
            } else {
                permissionAction.requestMorePermissions(new String[]{permissionModels[0].getPermission(), permissionModels[1].getPermission()}, permissionModels[0].getCode());
            }
        }
    }

    private void requestPermission(PermissionModel permissionModel){
        permissionAction.requestPermission(permissionModel.getPermission(), permissionModel.getCode());
    }

    public void checkGrantedPermission(int[] grantResult, int requestCode) {
        if (verifyGrantedPermission(grantResult)) {
            permissionCallbacks.permissionAccepted(requestCode);
        } else {
            permissionCallbacks.permissionDenied(requestCode);
        }
    }

    private boolean verifyGrantedPermission(int[] grantResults){
        for (int result : grantResults){
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void requestLocationStoragePermission() {
        checkAndRequestPermissions(new PermissionModel[]{PermissionModel.LOCATION, PermissionModel.STORAGE});
    }

    @Override
    public void requestPhonePermission() {
        checkAndRequestPermission(PermissionModel.PHONE);
    }

    @Override
    public void requestPhonePermissionAfterRationale() {
        requestPermission(PermissionModel.PHONE);
    }

    public interface PermissionCallbacks {

        void permissionAccepted(int actionCode);

        void permissionDenied(int actionCode);

        void showRationale(int actionCode);

    }

}
