package com.marko.kurt.easydrive.presentation.dagger.application.module;

import com.marko.kurt.easydrive.data.db.dao.EventDao;
import com.marko.kurt.easydrive.data.db.dao.UserDao;
import com.marko.kurt.easydrive.data.net.EventApi;
import com.marko.kurt.easydrive.data.net.LoginApi;
import com.marko.kurt.easydrive.data.repository.EventRepositoryImpl;
import com.marko.kurt.easydrive.data.repository.GetLocalUserRepositoryImpl;
import com.marko.kurt.easydrive.data.repository.UserRepositoryImpl;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.data.util.TelephonIdGenerator;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapper;
import com.marko.kurt.easydrive.domain.interactor.events.CreateEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.DislikeEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.GetEventsUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.GetOwnEventsUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.LikeEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.ActivateCodeUsecase;
import com.marko.kurt.easydrive.domain.interactor.user.CheckActivationCodeStatusUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.GetLocalUserUsecase;
import com.marko.kurt.easydrive.domain.interactor.user.LoginUsecase;
import com.marko.kurt.easydrive.domain.interactor.user.RegistrationUserUseCase;
import com.marko.kurt.easydrive.domain.repository.EventRepository;
import com.marko.kurt.easydrive.domain.repository.LocalUserRepository;
import com.marko.kurt.easydrive.domain.repository.UserRepository;
import com.marko.kurt.easydrive.presentation.dagger.activity.MapActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.broadcastReceiver.LikeBroadcastReceiverComponent;

import org.osmdroid.bonuspack.routing.RoadManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by user1 on 12.8.2017..
 */
@Module(subcomponents = {MapActivityComponent.class, LikeBroadcastReceiverComponent.class})
public class UseCaseModule {

    @Provides
    @Singleton
    EventRepository provideEventRepository(@Named("event_service") EventApi eventApi,
                                           FileManager fileManager, TelephonIdGenerator telephoneGenerator,
                                           ConnectivityManagerWrapper connectivityManagerWrapper, EventDao eventDao,
                                           RoadManager roadManager){
        return new EventRepositoryImpl(eventApi, fileManager, telephoneGenerator, connectivityManagerWrapper, eventDao, roadManager);
    }

    @Provides
    @Singleton
    GetEventsUseCase provideGetEventsUserCase(CompositeDisposable cOmpositeDIsposable, EventRepository eventRepository){
        return new GetEventsUseCase(cOmpositeDIsposable, eventRepository);
    }

    @Provides
    @Singleton
    UserRepository provideuserRepository(@Named("login_service") LoginApi loginApi,
                                         TelephonIdGenerator telephonIdGenerator, FileManager fileManager,
                                         ConnectivityManagerWrapper wrapper, UserDao userDao){
        return new UserRepositoryImpl(loginApi, telephonIdGenerator, fileManager, wrapper, userDao);
    }

    @Provides
    @Singleton
    SingleUseCase provideCheckStatusUseCase(CompositeDisposable compositeDisposable, UserRepository userRepository){
        return new CheckActivationCodeStatusUseCase(compositeDisposable, userRepository);
    }

    @Provides
    @Singleton
    ActivateCodeUsecase provideActivationCodeUsecase(CompositeDisposable compositeDisposable, UserRepository userRepository){
        return new ActivateCodeUsecase(compositeDisposable, userRepository);
    }

    @Provides
    @Singleton
    SingleUseCase provideLoginUseCase(CompositeDisposable compositeDisposable, UserRepository userRepository){
        return new LoginUsecase(compositeDisposable, userRepository);
    }

    @Provides
    @Singleton
    SingleUseCase provideRegistrationUseCase(CompositeDisposable compositeDisposable, UserRepository userRepository){
        return new RegistrationUserUseCase(compositeDisposable, userRepository);
    }

    @Provides
    @Singleton
    CreateEventUseCase provideCreateEventuseCase(CompositeDisposable compositeDisposable, EventRepository eventRepository){
        return new CreateEventUseCase(compositeDisposable, eventRepository);
    }

    @Provides
    @Singleton
    LocalUserRepository provideLocaluserRepository(FileManager fileManager){
        return new GetLocalUserRepositoryImpl(fileManager);
    }

    @Provides
    @Singleton
    GetLocalUserUsecase provideLocalUserUsecase(CompositeDisposable compositeDisposable, LocalUserRepository localUserRepository){
        return new GetLocalUserUsecase(compositeDisposable, localUserRepository);
    }

    @Provides
    @Singleton
    GetOwnEventsUseCase provideGetOwnEventsUseCase(){
        return new GetOwnEventsUseCase();
    }

    @Provides
    @Singleton
    SingleUseCase provideLikeEventUseCase(CompositeDisposable compositeDisposable, EventRepository eventRepository){
        return new LikeEventUseCase(compositeDisposable, eventRepository);
    }

    @Provides
    @Singleton
    SingleUseCase provideDislikeEventUsecase(CompositeDisposable compositeDisposable, EventRepository eventRepository){
        return new DislikeEventUseCase(compositeDisposable, eventRepository);
    }

}
