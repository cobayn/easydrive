package com.marko.kurt.easydrive.presentation.dagger.fragment;

import com.marko.kurt.easydrive.presentation.dagger.fragment.module.ActivationCodeModule;
import com.marko.kurt.easydrive.presentation.view.fragment.ActivationCodeFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 2.9.2017..
 */
@Subcomponent(modules = ActivationCodeModule.class)
public interface ActivationCodeComponent extends AndroidInjector<ActivationCodeFragment>{

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ActivationCodeFragment>{}

}
