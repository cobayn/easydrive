package com.marko.kurt.easydrive.data.util;

import android.os.Build;

/**
 * Created by nsoft on 21/08/2017.
 */

public class CommonUtils {

    public static boolean isMarshmallowOrHigher() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            return false;
        } else {
            return true;
        }

    }

}
