package com.marko.kurt.easydrive.presentation.exception;

import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;

/**
 * Created by user1 on 5.11.2017..
 */

public class ErrorMessageFactory {

    public static String create(Exception exception){

        String message = "No internet connection";

        if (exception instanceof NetworkConnectionException && exception.getMessage() == null){
            message = "No internet connection";
        } else {
            message = exception.getCause().getMessage();
        }

        return message;

    }

}
