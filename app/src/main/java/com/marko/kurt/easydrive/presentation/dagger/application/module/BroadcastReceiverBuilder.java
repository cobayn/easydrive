package com.marko.kurt.easydrive.presentation.dagger.application.module;

import android.content.BroadcastReceiver;

import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcaseReceiver;
import com.marko.kurt.easydrive.presentation.dagger.broadcastReceiver.LikeBroadcastReceiverComponent;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.BroadcastReceiverKey;
import dagger.multibindings.IntoMap;

/**
 * Created by nsoft on 19/12/2017.
 */
@Module
public abstract class BroadcastReceiverBuilder {

    @Binds
    @IntoMap
    @BroadcastReceiverKey(LikeBroadcaseReceiver.class)
    abstract AndroidInjector.Factory<? extends BroadcastReceiver> bindLikeBroadcastReceiver(LikeBroadcastReceiverComponent.Builder builder);


}
