package com.marko.kurt.easydrive.presentation.model;

/**
 * Created by nsoft on 08/05/2017.
 */

public class UserModel {

    private String email, password, username;
    private short userType;
    private int id;

    public UserModel(){

    }

    public UserModel(String email, String password){
        this.email = email;
        this.password = password;
    }

    public UserModel(String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public UserModel(String email){
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public short getUserType() {
        return userType;
    }

    public void setUserType(short userType) {
        this.userType = userType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
