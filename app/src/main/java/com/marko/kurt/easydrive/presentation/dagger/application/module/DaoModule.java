package com.marko.kurt.easydrive.presentation.dagger.application.module;

import com.marko.kurt.easydrive.data.db.AppDatabase;
import com.marko.kurt.easydrive.data.db.dao.EventDao;
import com.marko.kurt.easydrive.data.db.dao.UserDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 27.11.2017..
 */

@Module
public class DaoModule {

    @Provides
    @Singleton
    EventDao provideEventDao(AppDatabase database){
        return database.getEventDao();
    }

    @Provides
    @Singleton
    UserDao provideUserDao(AppDatabase database){

        return database.getUserDao();

    }

}
