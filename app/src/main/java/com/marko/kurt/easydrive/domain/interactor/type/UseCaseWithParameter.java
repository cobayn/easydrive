package com.marko.kurt.easydrive.domain.interactor.type;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by user1 on 12.8.2017..
 */

public interface UseCaseWithParameter<P, R> {

    void execute(DisposableObserver<P> observer, R param);

}
