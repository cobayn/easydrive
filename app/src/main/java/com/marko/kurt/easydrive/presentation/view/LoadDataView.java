package com.marko.kurt.easydrive.presentation.view;

/**
 * Created by nsoft on 01/07/2017.
 */

public interface LoadDataView {

    public void showLoading();

    public void hideLoading();

    public void showError(String error);

}
