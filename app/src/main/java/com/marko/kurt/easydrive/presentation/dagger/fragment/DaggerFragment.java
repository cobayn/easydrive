package com.marko.kurt.easydrive.presentation.dagger.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.marko.kurt.easydrive.presentation.dagger.ComponentFactory;
import com.marko.kurt.easydrive.presentation.dagger.activity.DaggerActivity;

import dagger.android.support.AndroidSupportInjection;

/**
 * Created by user1 on 12.8.2017..
 */

public abstract class DaggerFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }
}
