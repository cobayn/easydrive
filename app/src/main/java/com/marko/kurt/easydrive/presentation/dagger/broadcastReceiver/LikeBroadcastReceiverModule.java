package com.marko.kurt.easydrive.presentation.dagger.broadcastReceiver;

import com.marko.kurt.easydrive.domain.interactor.events.DislikeEventUseCase;
import com.marko.kurt.easydrive.domain.interactor.events.LikeEventUseCase;
import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcaseReceiver;
import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcastReceiverView;
import com.marko.kurt.easydrive.presentation.presenter.event.BroadcastReceiverPresenter;
import com.marko.kurt.easydrive.presentation.presenter.event.BroadcastReceiverPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by nsoft on 19/12/2017.
 */

@Module
public class LikeBroadcastReceiverModule {

    @Provides
    public LikeBroadcastReceiverView provideBroadcastView(LikeBroadcaseReceiver likeBroadcaseReceiver){
        return likeBroadcaseReceiver;
    }

    @Provides
    public BroadcastReceiverPresenter providePresenter(LikeBroadcastReceiverView view, LikeEventUseCase likeEventUseCase,
                                                       DislikeEventUseCase dislikeEventUseCase){
        return new BroadcastReceiverPresenterImpl(view, likeEventUseCase, dislikeEventUseCase);
    }

}
