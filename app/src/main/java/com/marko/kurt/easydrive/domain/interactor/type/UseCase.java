package com.marko.kurt.easydrive.domain.interactor.type;

import io.reactivex.Observable;

/**
 * Created by nsoft on 01/07/2017.
 */

public interface UseCase<T>{

    Observable<T> execute(double lat, double lng);
}
