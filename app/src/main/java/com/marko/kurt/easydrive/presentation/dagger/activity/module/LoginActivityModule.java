package com.marko.kurt.easydrive.presentation.dagger.activity.module;

import com.marko.kurt.easydrive.domain.interactor.user.CheckActivationCodeStatusUseCase;
import com.marko.kurt.easydrive.presentation.dagger.fragment.ActivationCodeComponent;
import com.marko.kurt.easydrive.presentation.dagger.fragment.LoginFragmentComponent;
import com.marko.kurt.easydrive.presentation.dagger.fragment.RegistrationFragmentComponent;
import com.marko.kurt.easydrive.presentation.dagger.fragment.RepairFragmentComponent;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginActivityPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginActivityPresenterImpl;
import com.marko.kurt.easydrive.presentation.view.LoginActivityView;
import com.marko.kurt.easydrive.presentation.view.activity.LoginActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 2.9.2017..
 */
@Module(subcomponents = {ActivationCodeComponent.class, LoginFragmentComponent.class, RegistrationFragmentComponent.class, RepairFragmentComponent.class})
public class LoginActivityModule extends ActivityModule<LoginActivity>{

    @Provides
    LoginActivityView provideLoginView(LoginActivity loginActivity){
        return loginActivity;
    }

    @Provides
    LoginActivityPresenter provideLoginActivityPresenter(LoginActivity loginActivity, CheckActivationCodeStatusUseCase useCase){
        return new LoginActivityPresenterImpl(loginActivity, useCase);
    }

}
