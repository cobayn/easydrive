package com.marko.kurt.easydrive.presentation.dagger.activity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by user1 on 19.8.2017..
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ForActivity {
}
