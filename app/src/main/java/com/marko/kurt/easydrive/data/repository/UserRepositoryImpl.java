package com.marko.kurt.easydrive.data.repository;

import com.marko.kurt.easydrive.data.db.dao.UserDao;
import com.marko.kurt.easydrive.data.entity.UserEntity;
import com.marko.kurt.easydrive.data.entity.mapper.ServerLoginReponseDataMapper;
import com.marko.kurt.easydrive.data.entity.mapper.UserEntityDataMapper;
import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.data.net.LoginApi;
import com.marko.kurt.easydrive.data.util.AppUtils;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.data.util.TelephonIdGenerator;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapper;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import java.net.MalformedURLException;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;

/**
 * Created by nsoft on 02/07/2017.
 */

public class UserRepositoryImpl implements UserRepository {

    private LoginApi loginApi;
    private ServerLoginReponseDataMapper mapper;
    private UserEntityDataMapper userMapper;
    private TelephonIdGenerator thelephoneGenerator;
    private FileManager fileManager;
    private Serializer mSerializer;
    private ConnectivityManagerWrapper connectivityManagerWrapper;

    public UserRepositoryImpl(LoginApi loginApi, TelephonIdGenerator idGenerator, FileManager filemanager,
                              ConnectivityManagerWrapper wrapper, UserDao userDao){
        this.loginApi = loginApi;
        this.thelephoneGenerator = idGenerator;
        this.fileManager = filemanager;
        this.connectivityManagerWrapper = wrapper;
        mapper = new ServerLoginReponseDataMapper(new Serializer());
        userMapper = new UserEntityDataMapper(new Serializer());
    }

    @Override
    public Single<ServerLoginReponse> activationCode(String authentication_code) {
        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            return loginApi.activateCode(authentication_code, thelephoneGenerator.newId())
                    .map(serverLoginResponseEntity -> mapper.transformResponse(serverLoginResponseEntity))
                    .doOnSuccess(new Consumer<ServerLoginReponse>() {
                        @Override
                        public void accept(ServerLoginReponse serverLoginReponse) throws Exception {
                            fileManager.writeToPreference(AppUtils.APPLICATION_MODE_KEY, authentication_code);
                        }
                    });
        } else {
            return Single.create(emitter ->
                emitter.onError(new NetworkConnectionException())
            );
        }
    }

    @Override
    public Single<User> loginUser(String username, String password, boolean remember_me) {

        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            try {
                return localLoginUser(username, password, remember_me);
            } catch (Exception e) {
                return Single.create(emitter -> {
                    emitter.onError(new NetworkConnectionException());
                });
            }
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }

    }

    @Override
    public Single<User> loginAnonymusUser() {
        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            return loginApi.loginAnonymusUser(fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY),
                    new UserEntity(thelephoneGenerator.newId())).map(serverLoginResponseEntityResponse -> {
                if (serverLoginResponseEntityResponse.isSuccessful())
                    fileManager.writeToPreference(AppUtils.SESSION_ID, serverLoginResponseEntityResponse.headers().get(AppUtils.SESSION_ID));
                return userMapper.transformLoginResponse(serverLoginResponseEntityResponse);
            }).doOnSuccess(new Consumer<User>() {
                @Override
                public void accept(User user) throws Exception {
                    fileManager.writeToPreference(AppUtils.EMAIL, "Anonymus");
                    fileManager.writeIntToPreference(AppUtils.USER_TYPE, user.getUserType());
                }
            });
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public Single<ServerLoginReponse> registerUser(String email, String username, String password) {
        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            return loginApi.registerUser(AppUtils.HEADER_CONTENT_TYPE, fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY),
                    thelephoneGenerator.newId(), new UserEntity(email, username, password))
                    .map(serverLoginResponseEntity -> mapper.transformResponse(serverLoginResponseEntity));
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public Observable<ServerLoginReponse> repairPassword(String password) {
        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            return loginApi.repairPassword(AppUtils.HEADER_CONTENT_TYPE, fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY),
                    thelephoneGenerator.newId(), password).map(
                    serverLoginResponseEntity -> mapper.transform(serverLoginResponseEntity));
        } else {
            return Observable.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public Single<ServerLoginReponse> checkCodeStatus(){
        if (connectivityManagerWrapper.isConnectedToNetwork()) {
            if (fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY).equals("")) {
                return Single.create(emitter -> {
                    emitter.onError(new NetworkConnectionException(new Throwable("No code")));
                });
            }
            return loginApi.checkStatusOfCode(fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY),
                    thelephoneGenerator.newId()).map(serverLoginResponseEntity -> {
                        //serverLoginResponseEntity.body().setUserLoggedIn(fileManager.getBooleanFromPreference(AppUtils.REMEBER_USER));
                        return mapper.transformResponse(serverLoginResponseEntity);
                    }).doOnSuccess(new Consumer<ServerLoginReponse>() {
                @Override
                public void accept(ServerLoginReponse serverLoginReponse) throws Exception {
                    serverLoginReponse.setUserRemember(fileManager.getBooleanFromPreference(AppUtils.REMEBER_USER));
                }
            });
        } else {
            return Single.create(emitter -> {
                emitter.onError(new NetworkConnectionException());
            });
        }
    }

    private Single<User> localLoginUser(String username, String password,
                                                      boolean remember_me) throws MalformedURLException{

        return loginApi.loginUser(AppUtils.HEADER_CONTENT_TYPE, fileManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY),
                thelephoneGenerator.newId(), new UserEntity(username, password))
                .map(serverLoginResponseEntity -> {
                    if (serverLoginResponseEntity.isSuccessful()) {
                        fileManager.writeToPreference(AppUtils.SESSION_ID, serverLoginResponseEntity.headers().get(AppUtils.SESSION_ID));
                    }
                    return userMapper.transformLoginResponse(serverLoginResponseEntity);
                }).doOnSuccess(user -> {
                    fileManager.writeToPreference(AppUtils.EMAIL, user.getEmail());
                    fileManager.writeIntToPreference(AppUtils.USER_TYPE, user.getUserType());
                    fileManager.writeBooleanToPreference(AppUtils.REMEBER_USER, remember_me);
                });

    }

}
