package com.marko.kurt.easydrive.presentation.router;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.view.activity.InfoActivity;
import com.marko.kurt.easydrive.presentation.view.activity.MapActivity;
import com.marko.kurt.easydrive.presentation.view.activity.SettingsActivity;
import com.marko.kurt.easydrive.presentation.view.fragment.ActivationCodeFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.LoginFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.RegistrationFragment;
import com.marko.kurt.easydrive.presentation.view.fragment.RepairFragment;

import javax.inject.Inject;

/**
 * Created by nsoft on 21/08/2017.
 */

public class RouterImpl implements Router {

    private static final int LAST_FRAGMENT = 0;

    private final Activity activity;
    private final FragmentManager fragmentManager;

    @Inject
    public RouterImpl(Activity activity, FragmentManager fragmentManager){
        this.activity = activity;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void closeScreen() {
        activity.finish();
    }

    @Override
    public void goBack() {

    }

    @Override
    public void startMapActivity() {
        Intent intent = new Intent(activity, MapActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public void openActivationFragment() {
        fragmentManager.beginTransaction().replace(R.id.content_login, new ActivationCodeFragment()).commit();
    }

    @Override
    public void openRegistrationFragment() {
        fragmentManager.beginTransaction().replace(R.id.content_login, new RegistrationFragment()).commit();
    }

    @Override
    public void openLoginFragment() {
        fragmentManager.beginTransaction().replace(R.id.content_login, new LoginFragment()).commit();
    }

    @Override
    public void openRepairPasswordFragment() {
        fragmentManager.beginTransaction().replace(R.id.content_login, new RepairFragment()).commit();
    }

    @Override
    public void openSettingsActivity() {
        activity.startActivity(new Intent(activity, SettingsActivity.class));
    }

    @Override
    public void openPermissionSettingsActivity() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);
    }

    @Override
    public void openInfoActivity() {
        activity.startActivity(new Intent(activity, InfoActivity.class));
    }
}
