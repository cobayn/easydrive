package com.marko.kurt.easydrive.data.entity.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marko.kurt.easydrive.data.entity.UserEntity;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by nsoft on 02/07/2017.
 */

public class UserEntityJsonMapper {

    private final Gson gson;

    public UserEntityJsonMapper(){
        this.gson = new Gson();
    }

    public UserEntity transformUserEntity(String userJsonResponse){
        Type userEntityType = new TypeToken<UserEntity>() {}.getType();
        return this.gson.fromJson(userJsonResponse, userEntityType);
    }

    public List<UserEntity> transformUserEntityCollection(String userListJsonResponse){
        Type listOfUserEntityType = new TypeToken<UserEntity>() {}.getType();
        return this.gson.fromJson(userListJsonResponse, listOfUserEntityType);
    }

}
