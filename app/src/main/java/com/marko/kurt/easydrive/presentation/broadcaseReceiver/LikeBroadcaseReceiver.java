package com.marko.kurt.easydrive.presentation.broadcaseReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.CallSuper;
import android.util.Log;
import android.widget.Toast;

import com.marko.kurt.easydrive.presentation.presenter.event.BroadcastReceiverPresenter;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/**
 * Created by user1 on 2.12.2017..
 */

public class LikeBroadcaseReceiver extends BroadcastReceiver implements LikeBroadcastReceiverView{

    @Inject
    BroadcastReceiverPresenter presenter;

    private Context mContext;

    @CallSuper
    @Override
    public void onReceive(Context context, Intent intent) {

        AndroidInjection.inject(this, context);
        this.mContext = context;

        if (intent.getAction() != null) {
            switch (intent.getAction()){

                case "like":
                    presenter.likeEvent((short)1);
                    break;
                case "dislike":
                    presenter.dislikeEvent((short)2);
                    break;
                default:
                    Log.i("Bezveze", "bezveze");

            }
        }

    }

    private void toastMessage(String message){
        Toast.makeText(this.mContext, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String error) {
        toastMessage(error);
    }

    @Override
    public void showSuccesfullMessage(String message) {
        toastMessage(message);
    }
}
