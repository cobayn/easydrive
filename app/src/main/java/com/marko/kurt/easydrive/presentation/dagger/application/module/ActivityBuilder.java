package com.marko.kurt.easydrive.presentation.dagger.application.module;

import android.app.Activity;

import com.marko.kurt.easydrive.presentation.dagger.activity.LoginActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.activity.MapActivityComponent;
import com.marko.kurt.easydrive.presentation.view.activity.LoginActivity;
import com.marko.kurt.easydrive.presentation.view.activity.MapActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by user1 on 2.9.2017..
 */
@Module
public abstract class ActivityBuilder {

    @Binds
    @IntoMap
    @ActivityKey(LoginActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindLoginActivity(LoginActivityComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(MapActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> bindMapActivity(MapActivityComponent.Builder builder);

}
