package com.marko.kurt.easydrive.presentation.dagger.application.module;

import com.marko.kurt.easydrive.presentation.mapper.EventViewModeMapper;
import com.marko.kurt.easydrive.presentation.mapper.EventViewModeMapperImpl;
import com.marko.kurt.easydrive.presentation.mapper.UserViewModelMapper;
import com.marko.kurt.easydrive.presentation.mapper.UserViewModelMapperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 12.8.2017..
 */
@Module
public class MapperModule {

    @Provides
    @Singleton
    EventViewModeMapper provideEventViewModeMapper(){
        return new EventViewModeMapperImpl();
    }

    @Provides
    @Singleton
    UserViewModelMapper provideUserViewModeMapper(){
        return new UserViewModelMapperImpl();
    }

}
