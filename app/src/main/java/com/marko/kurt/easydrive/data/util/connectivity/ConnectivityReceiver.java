package com.marko.kurt.easydrive.data.util.connectivity;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by user1 on 19.8.2017..
 */

public interface ConnectivityReceiver {

    Observable<Boolean> getConnectivityStatus();

    Single<Boolean> isConnected();

}
