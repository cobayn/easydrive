package com.marko.kurt.easydrive.presentation.view.activity;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginActivityPresenter;
import com.marko.kurt.easydrive.presentation.util.OnControlSnackbarListener;
import com.marko.kurt.easydrive.presentation.view.LoginActivityView;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class LoginActivity extends BaseActivity implements LoginActivityView,OnControlSnackbarListener,
         HasSupportFragmentInjector{

    @BindView(R.id.checking_code_progressbar)
    ProgressBar checking_status_progressbar;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    LoginActivityPresenter loginActivityPresenter;

    @Override
    public void showLoading() {
        checking_status_progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        checking_status_progressbar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        showErrorSnackbar(error);
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void init() {
        //router.startMapActivity();
        permissionPresenter.requestPhonePermission();
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return loginActivityPresenter;
    }

    @Override
    protected View getSnackbarView() {
        return findViewById(R.id.login_cordinatorLayout);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void showErrorSnackbar(String message) {
        snackIt(message, R.color.errorSnacbar);
    }

    @Override
    public void showSuccessfullSnackbar(String message) {
        snackIt(message, R.color.successfullSnackbar);
    }

    @Override
    public void openActivationFragment() {
        router.openActivationFragment();
    }

    @Override
    public void openLoginFragment() {
        router.openLoginFragment();
    }

    @Override
    public void finishApplicationWork(){
        this.finish();
    }

    @Override
    public void permissionAccepted(int actionCode) {
        loginActivityPresenter.checkStatusOfCode();
    }

    @Override
    public void openMapActivity() {
        router.startMapActivity();
    }
}