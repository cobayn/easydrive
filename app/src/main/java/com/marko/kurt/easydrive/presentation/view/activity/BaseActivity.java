package com.marko.kurt.easydrive.presentation.view.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.dagger.activity.DaggerActivity;
import com.marko.kurt.easydrive.presentation.model.PermissionModel;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.presenter.permission.PermissionPresenter;
import com.marko.kurt.easydrive.presentation.presenter.permission.PermissionPresenterImpl;
import com.marko.kurt.easydrive.presentation.router.Router;
import com.marko.kurt.easydrive.presentation.view.LoadDataView;
import com.marko.kurt.easydrive.presentation.view.fragment.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by nsoft on 08/04/2017.
 */

public abstract class BaseActivity extends DaggerActivity implements LoadDataView, PermissionPresenterImpl.PermissionCallbacks{

    protected abstract int setLayout();
    protected abstract void init();
    protected abstract ScopedPresenter getPresenter();
    protected abstract View getSnackbarView();

    @Inject
    PermissionPresenter permissionPresenter;
    @Inject
    Router router;
    @Inject
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
        ButterKnife.bind(this);
        init();
        getPresenter().start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().activate();
    }

    @Override
    protected void onPause() {
        getPresenter().deactivate();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()){
            getPresenter().destroy();
            clearFragments();
        }
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionModel.PERMISSION_PHONE:
            case PermissionModel.PERMISSION_STORAGE:
            case PermissionModel.PERMISSION_LOCATION:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
        }
    }

    private void clearFragments(){
        final List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (final Fragment fragment : fragments){
            if (fragment instanceof BaseFragment){
                ((BaseFragment) fragment).onPreDestroy();
            }
        }
    }

    protected void snackIt(CharSequence message, int resIdColor){
        Snackbar sb = Snackbar.make(getSnackbarView(), message, Snackbar.LENGTH_SHORT);
        sb.getView().setBackgroundColor(this.getResources().getColor(resIdColor));
        sb.show();
    }

    private void snackWarningPermission(){
        Snackbar sb = Snackbar.make(getSnackbarView(), getString(R.string.rationale_accept_permissions), Snackbar.LENGTH_INDEFINITE);
        sb.setAction(R.string.rationale_settings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                router.openPermissionSettingsActivity();
            }
        });
        sb.show();
    }

    @Override
    public void permissionDenied(int actionCode) {
        snackWarningPermission();
    }

    @Override
    public void showRationale(int actionCode) {
        snackWarningPermission();
    }
}
