package com.marko.kurt.easydrive.presentation.mapper;

import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.presentation.model.UserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by kurt on 14/11/2017.
 */

public class UserViewModelMapperImpl implements UserViewModelMapper{
    @Override
    public UserModel transform(User user) {
        if (user == null){
            throw new IllegalArgumentException("");
        }
        UserModel userModel = new UserModel();
        userModel.setUsername(user.getUsername());
        userModel.setPassword(user.getPassword());
        userModel.setEmail(user.getEmail());
        userModel.setUserType(user.getUserType());
        return userModel;
    }

    @Override
    public Collection<UserModel> transform(Collection<User> userList) {
        Collection<UserModel> userModelCollection;

        if (userList != null && !userList.isEmpty()){
            userModelCollection = new ArrayList<>();
            for (User user : userList) {
                userModelCollection.add(transform(user));
            }
        } else {
            userModelCollection = Collections.emptyList();
        }
        return userModelCollection;
    }
}
