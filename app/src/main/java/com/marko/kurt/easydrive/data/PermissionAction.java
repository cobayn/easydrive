package com.marko.kurt.easydrive.data;

/**
 * Created by nsoft on 21/08/2017.
 */

public interface PermissionAction {

    boolean hasSelfPermission(String permission);

    void requestPermission(String permission, int requestCode);

    void requestMorePermissions(String[] permissions, int requestCode);

    boolean shouldShowRequestPermissionRationale(String permission);

}
