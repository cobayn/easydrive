package com.marko.kurt.easydrive.presentation.view;

/**
 * Created by nsoft on 21/04/2017.
 */

public interface RepairFragmentView extends LoadDataView{

    public void showMessage(String message);

}
