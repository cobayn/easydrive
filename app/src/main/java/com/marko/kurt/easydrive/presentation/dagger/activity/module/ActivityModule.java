package com.marko.kurt.easydrive.presentation.dagger.activity.module;

import android.support.v4.app.FragmentManager;

import com.marko.kurt.easydrive.data.PermissionAction;
import com.marko.kurt.easydrive.data.impl.SupportPermissionActionImpl;
import com.marko.kurt.easydrive.presentation.presenter.permission.PermissionPresenter;
import com.marko.kurt.easydrive.presentation.presenter.permission.PermissionPresenterImpl;
import com.marko.kurt.easydrive.presentation.router.Router;
import com.marko.kurt.easydrive.presentation.router.RouterImpl;
import com.marko.kurt.easydrive.presentation.view.activity.BaseActivity;
import com.marko.kurt.easydrive.presentation.view.activity.LoginActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 18.8.2017..
 */
@Module
public abstract class ActivityModule<T extends BaseActivity> {

    @Provides
    FragmentManager provideFragmentManager(T baseActivity){
        return baseActivity.getSupportFragmentManager();
    }

    @Provides
    Router provideRouter(T baseActivity, FragmentManager fragmentManager){
        return new RouterImpl(baseActivity, fragmentManager);
    }

    @Provides
    PermissionAction providePermissionAction(T baseActivity){
        return new SupportPermissionActionImpl(baseActivity);
    }

    @Provides
    PermissionPresenter providePermissionPresenter(PermissionAction permissionAction, T baseActivity){
        return new PermissionPresenterImpl(permissionAction, baseActivity);
    }

}
