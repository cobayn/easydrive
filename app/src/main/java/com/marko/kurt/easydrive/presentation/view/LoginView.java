package com.marko.kurt.easydrive.presentation.view;

/**
 * Created by nsoft on 21/04/2017.
 */

public interface LoginView extends LoadDataView{

    void onSuccess();

    void hideAnonymusDetails();

    void showAnonymusDetails();

    void setUsernameError();

    void setPasswordError();

}
