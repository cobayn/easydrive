package com.marko.kurt.easydrive.presentation.model;

/**
 * Created by nsoft on 06/09/2017.
 */

public class ServerLoginResponseModel {

    private String message, type;
    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
