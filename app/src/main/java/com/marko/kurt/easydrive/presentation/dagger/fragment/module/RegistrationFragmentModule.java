package com.marko.kurt.easydrive.presentation.dagger.fragment.module;

import com.marko.kurt.easydrive.domain.interactor.user.RegistrationUserUseCase;
import com.marko.kurt.easydrive.presentation.presenter.login.RegistrationPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.RegistrationPresenterImpl;
import com.marko.kurt.easydrive.presentation.view.RegistrationView;
import com.marko.kurt.easydrive.presentation.view.fragment.RegistrationFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 21.9.2017..
 */
@Module
public class RegistrationFragmentModule {

    @Provides
    RegistrationView provideRegistrationView(RegistrationFragment registrationFragment){
        return registrationFragment;
    }

    @Provides
    RegistrationPresenter provideRegistrationPresenter(RegistrationFragment registrationFragment, RegistrationUserUseCase loginUsecase){
        return new RegistrationPresenterImpl(registrationFragment, loginUsecase);
    }

}
