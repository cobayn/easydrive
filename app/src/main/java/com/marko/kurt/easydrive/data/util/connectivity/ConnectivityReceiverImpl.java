package com.marko.kurt.easydrive.data.util.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by user1 on 19.8.2017..
 */

public class ConnectivityReceiverImpl extends BroadcastReceiver implements ConnectivityReceiver {

    private static final String TAG = ConnectivityReceiverImpl.class.getSimpleName();

    private static final String ACTION_CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";

    private final NetworkUtils networkUtils;
    private final Scheduler backgroundScheduler;
    private final PublishSubject<Boolean> subject;

    public ConnectivityReceiverImpl(final Context context, final NetworkUtils networkUtils, final Scheduler backgroundScheduler){
        this.networkUtils = networkUtils;
        this.backgroundScheduler = backgroundScheduler;
        final IntentFilter intentFilter = new IntentFilter(ACTION_CONNECTIVITY_CHANGE);
        context.registerReceiver(this, intentFilter);
        this.subject = PublishSubject.create();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

    }

    @Override
    public Observable<Boolean> getConnectivityStatus() {
        return subject.subscribeOn(backgroundScheduler).observeOn(backgroundScheduler);
    }

    @Override
    public Single<Boolean> isConnected() {
        return null;
    }
}
