package com.marko.kurt.easydrive.data.entity;

/**
 * Created by nsoft on 06/09/2017.
 */

public class ServerLoginResponseEntity {

    private String message, type;
    private int code;
    private boolean isUserLoggedIn;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isUserLoggedIn() {
        return isUserLoggedIn;
    }

    public void setUserLoggedIn(boolean userLoggedIn) {
        isUserLoggedIn = userLoggedIn;
    }
}
