package com.marko.kurt.easydrive.presentation.util;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;

import org.jetbrains.annotations.NotNull;

/**
 * Created by user1 on 2.12.2017..
 */

public class NotificationsImpl implements Notifications {

    private final NotificationManagerCompat notificationManagerCompat;

    public NotificationsImpl(@NotNull final Context context){
        this.notificationManagerCompat = NotificationManagerCompat.from(context);
    }

    @Override
    public void showNotification(int notificationId, Notification notification) {
        notificationManagerCompat.notify(notificationId, notification);
    }

    @Override
    public void updateNotification(int notificationId, Notification notification) {
        notificationManagerCompat.notify(notificationId, notification);
    }

    @Override
    public void hideNotification(int notificationId) {
        notificationManagerCompat.cancel(notificationId);
    }

    @Override
    public void hideNotifications() {
        notificationManagerCompat.cancelAll();
    }
}
