package com.marko.kurt.easydrive.data.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by nsoft on 08/04/2017.
 */

public class FileManager {

    private Context mContext;

    public FileManager (Context context){
        this.mContext = context;
    }

    public void writeToPreference(String key, String value){

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();

    }

    public String getFromPreferences(String key){

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        return sharedPreferences.getString(key, "");

    }

    public void writeBooleanToPreference(String key, boolean value){

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();

    }

    public void writeIntToPreference(String key, int value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public boolean getBooleanFromPreference(String key){

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getBoolean(key, false);

    }

    public int getIntFromPreferences(String key){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getInt(key, 0);
    }

}
