package com.marko.kurt.easydrive.presentation.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by user1 on 25.9.2017..
 */

public class AppModeModel {

    @IntDef({MODE_WALK, MODE_DRIVE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface AppModeCode {}

    public static final int MODE_WALK = 0;
    public static final int MODE_DRIVE = 1;

    private int code;
    private String appMode;

    public static final AppModeModel WALK = new AppModeModel(MODE_WALK, "Walk");
    public static final AppModeModel DRIVE = new AppModeModel(MODE_DRIVE, "Drive");

    private AppModeModel(@AppModeCode int value, String appMode){
        this.code = value;
        this.appMode = appMode;
    }

    @AppModeCode
    public int getCode() {
        return code;
    }

    public String getAppMode() {
        return appMode;
    }
}
