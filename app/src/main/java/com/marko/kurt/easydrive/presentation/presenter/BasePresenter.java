package com.marko.kurt.easydrive.presentation.presenter;

import android.support.annotation.CallSuper;

import com.marko.kurt.easydrive.presentation.view.LoadDataView;

import java.lang.ref.WeakReference;

import io.reactivex.annotations.NonNull;

/**
 * Created by user1 on 19.8.2017..
 */

public abstract class BasePresenter<View extends LoadDataView> implements ScopedPresenter {

    private WeakReference<View> viewReference = new WeakReference<>(null);

    public BasePresenter(final View view){
        viewReference = new WeakReference<View>(view);
    }

    @Override
    @CallSuper
    public void start() {

    }

    @Override
    @CallSuper
    public void activate() {

    }

    @Override
    @CallSuper
    public void deactivate() {

    }

    @Override
    @CallSuper
    public void destroy() {

    }

    @Override
    public void back() {

    }

    @NonNull
    protected View getNullableView(){
        return viewReference.get();
    }

}
