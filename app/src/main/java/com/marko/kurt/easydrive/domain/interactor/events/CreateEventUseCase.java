package com.marko.kurt.easydrive.domain.interactor.events;

import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user1 on 25.11.2017..
 */

public class CreateEventUseCase {

    private EventRepository eventRepository;
    private CompositeDisposable compositeDisposable;

    public CreateEventUseCase(CompositeDisposable compositeDisposable, EventRepository eventRepository){
        this.compositeDisposable = compositeDisposable;
        this.eventRepository = eventRepository;
    }

    public void execute(double lat, double lng, short eventType, String description){
        compositeDisposable.add(eventRepository.createEvent(lat, lng, eventType, description)
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DeafultSingleObserver<Object>()));
    }

    public void deactivateUserCode(){
        eventRepository.deactivateUserCode();
    }

}
