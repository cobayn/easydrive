package com.marko.kurt.easydrive.presentation.dagger.fragment.module;

import com.marko.kurt.easydrive.domain.interactor.user.ActivateCodeUsecase;
import com.marko.kurt.easydrive.presentation.presenter.login.ActivationCodePresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.ActivationCodePresenterImpl;
import com.marko.kurt.easydrive.presentation.view.ActivationCodeView;
import com.marko.kurt.easydrive.presentation.view.fragment.ActivationCodeFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 2.9.2017..
 */
@Module
public class ActivationCodeModule {

    @Provides
    ActivationCodeView provideActivationCodeFragmentView(ActivationCodeFragment activationCodeFragment){
        return activationCodeFragment;
    }

    @Provides
    ActivationCodePresenter provideActivationCodePresenter(ActivationCodeFragment fragment, ActivateCodeUsecase usecase){
        return new ActivationCodePresenterImpl(fragment, usecase);
    }

}
