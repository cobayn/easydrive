package com.marko.kurt.easydrive.presentation.view.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginPresenter;
import com.marko.kurt.easydrive.presentation.router.Router;
import com.marko.kurt.easydrive.presentation.util.ActivityUtils;
import com.marko.kurt.easydrive.presentation.view.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginFragment extends BaseLoginFragment implements LoginView{

    @BindView(R.id.username_fragment_login_edittext)
    EditText username;
    @BindView(R.id.password_fragment_login_edittext)
    EditText password;
    @BindView(R.id.login_progressbar)
    ProgressBar pbar;
    @BindView(R.id.login_button)
    Button login;
    @BindView(R.id.anonymus_user_textview)
    Button anonymus;
    @BindView(R.id.anonymus_user_progressbar)
    ProgressBar anonymusProgressbar;
    @BindView(R.id.login_relativelayout)
    ConstraintLayout loginLayout;
    @BindView(R.id.anonymus_user_relativelayout)
    ConstraintLayout anonymusLayout;
    @BindView(R.id.username_fragment_login_inputlayout)
    TextInputLayout usernameLoginInputlayout;
    @BindView(R.id.password_fragment_login_inputlayout)
    TextInputLayout passwordLoginInputlayout;
    @BindView(R.id.remeber_me_checkbox)
    AppCompatCheckBox remeber_me;

    @Inject
    LoginPresenter presenter;
    @Inject
    Router router;

    @Override
    public void onSuccess() {
        router.startMapActivity();
    }

    @Override
    public void hideAnonymusDetails() {
        setEnabled(true);
        anonymusProgressbar.setVisibility(View.GONE);
        anonymus.setVisibility(View.VISIBLE);
    }

    @Override
    public void showAnonymusDetails() {
        setEnabled(false);
        anonymusProgressbar.setVisibility(View.VISIBLE);
        anonymus.setVisibility(View.GONE);
    }

    @Override
    public void setUsernameError() {
        usernameLoginInputlayout.setError("Username prazan");
    }

    @Override
    public void setPasswordError() {
        passwordLoginInputlayout.setError("Password prazan");
    }

    @Override
    public void showLoading() {
        setEnabled(false);
        pbar.setVisibility(View.VISIBLE);
        login.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        setEnabled(true);
        pbar.setVisibility(View.GONE);
        login.setVisibility(View.VISIBLE);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_login;
    }

    @Override
    protected void init(View view) {
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @OnClick(R.id.open_registration_textview)
    public void onOpenRegistrationFormClickListener(){
        activityUtils.removeFragmentFromActivity(fragmentManager, this);
        activityUtils.addFragmentToActivity(fragmentManager, new RegistrationFragment(), R.id.content_login);
    }

    @OnClick(R.id.login_relativelayout)
    public void onLoginClickListener(){
        presenter.validateInputs(username.getText().toString(), password.getText().toString(),
                remeber_me.isChecked());
    }

    @OnClick(R.id.anonymus_user_relativelayout)
    public void onAnonymusUserClickListener(){
        presenter.anonymusUser();
    }

    private void setEnabled(boolean enabled){
        username.setEnabled(enabled);
        password.setEnabled(enabled);
        loginLayout.setEnabled(enabled);
        anonymusLayout.setEnabled(enabled);
    }

}
