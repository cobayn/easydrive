package com.marko.kurt.easydrive.presentation.dagger.application.module;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.marko.kurt.easydrive.data.db.AppDatabase;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.data.util.TelephonIdGenerator;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapper;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapperImpl;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityReceiver;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityReceiverImpl;
import com.marko.kurt.easydrive.data.util.connectivity.NetworkUtils;
import com.marko.kurt.easydrive.data.util.connectivity.NetworkUtilsImpl;
import com.marko.kurt.easydrive.presentation.dagger.activity.LoginActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.activity.MapActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.application.ForApplication;
import com.marko.kurt.easydrive.presentation.util.ActivityUtils;
import com.marko.kurt.easydrive.presentation.util.ActivityUtilsImpl;
import com.marko.kurt.easydrive.presentation.view.activity.MapActivity;

import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.RoadManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user1 on 12.8.2017..
 */
@Module(subcomponents = {
        LoginActivityComponent.class,
        MapActivityComponent.class
})
public class UtilsModule {

    @Provides
    @Singleton
    ActivityUtils provideActivityUtils(){
        return new ActivityUtilsImpl();
    }

    @Provides
    @Singleton
    ConnectivityReceiver provideConnectivityReceiver(final @ForApplication Context context, final NetworkUtils networkUtils){
        return new ConnectivityReceiverImpl(context, networkUtils, Schedulers.io());
    }

    @Provides
    @Singleton
    NetworkUtils provideNetworkUtils(final ConnectivityManagerWrapper connectivityManagerWrapper){
        return new NetworkUtilsImpl(connectivityManagerWrapper);
    }

    /*@Provides
    @Singleton
    ConnectivityManagerWrapper provideConnectivityManagerWrapper(final @ForApplication Context context){
        return new ConnectivityManagerWrapperImpl(context);
    }*/

    @Provides
    @Singleton
    FileManager provideFileManager(Application app){
        return new FileManager(app);
    }

    @Provides
    @Singleton
    TelephonIdGenerator provideTelephoneIdGenerator(Application app){
        return new TelephonIdGenerator(app);
    }

    @Provides
    @Singleton
    ConnectivityManagerWrapper provideConnectivityWrapper(Application app){
        return new ConnectivityManagerWrapperImpl(app);
    }

    @Provides
    @Singleton
    AppDatabase provideRoomDatabase(Application app){
        return Room.databaseBuilder(app, AppDatabase.class, "easy_drive").allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    RoadManager provideRoadManager(Application application){
        return new OSRMRoadManager(application);
    }

}
