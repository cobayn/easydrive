package com.marko.kurt.easydrive.presentation.dagger.application.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.marko.kurt.easydrive.data.net.EventApi;
import com.marko.kurt.easydrive.data.net.LoginApi;
import com.marko.kurt.easydrive.presentation.dagger.activity.LoginActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.activity.MapActivityComponent;
import com.marko.kurt.easydrive.presentation.dagger.broadcastReceiver.LikeBroadcastReceiverComponent;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nsoft on 01/07/2017.
 */
@Module(subcomponents = {LoginActivityComponent.class, MapActivityComponent.class, LikeBroadcastReceiverComponent.class})
public class AppModule {

    private static final String BASE_URL = "";

    @Provides
    @Singleton
    Context provideContext(Application application){
        return application;
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideLoginInterceptor(){
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    public OkHttpClient provideClient(HttpLoggingInterceptor interceptor) {

        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient client){

        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder().baseUrl(BASE_URL).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    public CompositeDisposable provideCompositeDisposable(){
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    @Named("login_service")
    public LoginApi provideLoginRepository(Retrofit retrofit){
        return retrofit.create(LoginApi.class);
    }

    @Provides
    @Singleton
    @Named("event_service")
    public EventApi provideEventRepository(Retrofit retrofit){
        return retrofit.create(EventApi.class);
    }

}
