package com.marko.kurt.easydrive.presentation.presenter;

/**
 * Created by user1 on 19.8.2017..
 */

public interface ScopedPresenter {

    void start();

    void activate();

    void deactivate();

    void destroy();

    void back();

}
