package com.marko.kurt.easydrive.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.marko.kurt.easydrive.data.db.dao.EventDao;
import com.marko.kurt.easydrive.data.db.dao.UserDao;
import com.marko.kurt.easydrive.data.entity.EventEntity;
import com.marko.kurt.easydrive.data.entity.UserEntity;

/**
 * Created by user1 on 5.12.2017..
 */
@Database(entities = {EventEntity.class, UserEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{

    public abstract EventDao getEventDao();

    public abstract UserDao getUserDao();

}
