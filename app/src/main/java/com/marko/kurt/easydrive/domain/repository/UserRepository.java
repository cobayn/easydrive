package com.marko.kurt.easydrive.domain.repository;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.User;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by nsoft on 02/07/2017.
 */

public interface UserRepository {

    Single<ServerLoginReponse> activationCode(String authentication_code);

    Single<User> loginUser(String username, String password, boolean remeber_me);

    Single<User> loginAnonymusUser();

    Single<ServerLoginReponse> registerUser(String email, String username, String password);

    Observable<ServerLoginReponse> repairPassword(String password);

    Single<ServerLoginReponse> checkCodeStatus();

}
