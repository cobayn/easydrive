package com.marko.kurt.easydrive.presentation.presenter.permission;

/**
 * Created by nsoft on 21/08/2017.
 */

public interface PermissionPresenter {

    void requestLocationStoragePermission();

    void checkGrantedPermission(int[] grantResult, int requestCode);

    void requestPhonePermission();

    void requestPhonePermissionAfterRationale();

}
