package com.marko.kurt.easydrive.data.net;

import com.marko.kurt.easydrive.data.entity.CreateEventEntity;
import com.marko.kurt.easydrive.data.entity.LikeEventEntity;
import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by nsoft on 02/07/2017.
 */

public interface EventApi {

    String RADAR_ALL = "";

    String RADAR_LIKE = "";

    String RADAR_DISLIKE = "";

    String RADAR_CREATE = "";

    @GET(RADAR_ALL)
    public Observable<Response<Object>> getAllEvents(@QueryMap HashMap<String, Double> queries,
                                                     @HeaderMap HashMap<String, String> headers);

    @POST(RADAR_LIKE)
    public Single<Response<ServerLoginResponseEntity>> likeEvent(@HeaderMap HashMap<String, String> headers,
                                    @Body LikeEventEntity likeEventEntity);

    @POST(RADAR_DISLIKE)
    public Single<Response<ServerLoginResponseEntity>> dislikeEvent(@HeaderMap HashMap<String, String> headers,
                                                                    @Body LikeEventEntity dislikeEventEntity);

    @POST(RADAR_CREATE)
    public Single<Response<ServerLoginResponseEntity>> createEvent(@HeaderMap HashMap<String, String> headers,
                                                                   @Body CreateEventEntity createEventEntity);

}
