package com.marko.kurt.easydrive.presentation.util;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.view.activity.LoginActivity;

/**
 * Created by user1 on 2.12.2017..
 */

public class NotificationFactoryImpl implements NotificationFactory {

    private final Context context;

    public NotificationFactoryImpl(final Context context){
        this.context = context;
    }

    @Override
    public Notification createNewNotification(String warning_type, boolean isLiked, boolean isDisiked, boolean isMuted) {
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, "1");
        notificationBuilder.setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                //.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle())
                .addAction(R.mipmap.ic_launcher, context.getString(R.string.like_event), PendingIntent.getBroadcast(context, 0,  new Intent("like"), 0))
                .addAction(R.mipmap.ic_launcher, context.getString(R.string.dislike_event), PendingIntent.getBroadcast(context, 0,  new Intent("dislike"), 0))
                .addAction(R.mipmap.ic_launcher, context.getString(R.string.mute_event), PendingIntent.getActivity(context, 990,  new Intent("mute"), PendingIntent.FLAG_UPDATE_CURRENT))
                .setContentTitle(String.format(context.getResources().getString(R.string.notification_title), warning_type))
                .setContentText(String.format(context.getResources().getString(R.string.notification_message), warning_type));
        return notificationBuilder.build();
    }
}
