package com.marko.kurt.easydrive.presentation.dagger.fragment.module;

import com.marko.kurt.easydrive.domain.interactor.user.LoginUsecase;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.LoginPresenterImpl;
import com.marko.kurt.easydrive.presentation.view.fragment.LoginFragment;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user1 on 19.9.2017..
 */
@Module
public class LoginFragmentModule {

    @Provides
    LoginPresenter provideLoginPresenter(LoginFragment loginFragment, LoginUsecase usecase){
        return new LoginPresenterImpl(loginFragment, usecase);
    }

}
