package com.marko.kurt.easydrive.data.util.connectivity;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;

import io.reactivex.Single;

/**
 * Created by user1 on 19.8.2017..
 */

public class NetworkUtilsImpl implements NetworkUtils {

    private static final String EMPTY = "";
    private static final String PING_ADDRESS = "www.google.com";
    private final ConnectivityManagerWrapper connectivityManagerWrapper;

    public NetworkUtilsImpl(final ConnectivityManagerWrapper connectivityManagerWrapper){
        this.connectivityManagerWrapper = connectivityManagerWrapper;
    }

    @Override
    public Single<Boolean> isConnectedToInternet() {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return isConnectedToNetwork() && canResolveAddress(PING_ADDRESS);
            }
        });
    }

    @Override
    public Single<NetworkData> getActiveNetworkData() {
        return Single.fromCallable(new Callable<NetworkData>() {
            @Override
            public NetworkData call() throws Exception {
                return connectivityManagerWrapper.getNetworkData();
            }
        });
    }

    private boolean canResolveAddress(final String url){
        return pingAddress(url);
    }

    private boolean isConnectedToNetwork(){
        return connectivityManagerWrapper.isConnectedToNetwork();
    }

    private boolean pingAddress(final String url){
        try {
            final InetAddress address = InetAddress.getByName(url);
            return address != null && !EMPTY.equals(address.getHostAddress());
        } catch (final UnknownHostException e){
            return false;
        }
    }

}
