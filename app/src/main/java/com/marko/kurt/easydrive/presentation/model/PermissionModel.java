package com.marko.kurt.easydrive.presentation.model;

import android.Manifest;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by nsoft on 21/08/2017.
 */

public class PermissionModel {

    @IntDef({PERMISSION_LOCATION, PERMISSION_STORAGE, PERMISSION_PHONE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PermissionCode {}

    public static final int PERMISSION_LOCATION = 0;
    public static final int PERMISSION_STORAGE = 2;
    public static final int PERMISSION_PHONE = 3;

    public static final PermissionModel LOCATION = new PermissionModel(PERMISSION_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
    public static final PermissionModel STORAGE = new PermissionModel(PERMISSION_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    public static final PermissionModel PHONE = new PermissionModel(PERMISSION_PHONE, Manifest.permission.READ_PHONE_STATE);

    private int code;
    private String permission;

    private PermissionModel(@PermissionCode int value, String name){
        this.code = value;
        this.permission = name;
    }

    @PermissionCode
    public int getCode() {
        return code;
    }

    public String getPermission() {
        return permission;
    }
}
