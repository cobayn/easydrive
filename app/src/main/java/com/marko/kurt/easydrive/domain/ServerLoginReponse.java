package com.marko.kurt.easydrive.domain;

/**
 * Created by nsoft on 06/09/2017.
 */

public class ServerLoginReponse {

    private String message, type;
    private int code;
    private boolean isUserRemember;

    public ServerLoginReponse() {}

    public ServerLoginReponse(int code, String message){
        this.code = code;
        this.message = message;
    }

    public ServerLoginReponse(int code, String message, String type, boolean isRemember){
        this.code = code;
        this.message = message;
        this.type = type;
        this.isUserRemember = isRemember;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isUserRemember() {
        return isUserRemember;
    }

    public void setUserRemember(boolean userRemember) {
        isUserRemember = userRemember;
    }
}
