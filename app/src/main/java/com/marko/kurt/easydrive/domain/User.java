package com.marko.kurt.easydrive.domain;

/**
 * Created by nsoft on 01/07/2017.
 */

public class User {

    private int id;
    private String username, password, email;
    private short userType;

    public User(){}

    public User(int userType, String email){
        this.userType = (short)userType;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public short getUserType() {
        return userType;
    }

    public void setUserType(short userType) {
        this.userType = userType;
    }
}
