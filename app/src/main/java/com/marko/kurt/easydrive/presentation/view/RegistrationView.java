package com.marko.kurt.easydrive.presentation.view;

/**
 * Created by nsoft on 08/05/2017.
 */

public interface RegistrationView extends LoadDataView{

    public void onSuccess(String message);

    public void setEmailError();

    public void setMismatchPasswordError();

    public void setPasswordError();

}
