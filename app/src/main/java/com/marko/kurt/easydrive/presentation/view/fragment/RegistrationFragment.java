package com.marko.kurt.easydrive.presentation.view.fragment;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.presenter.login.RegistrationPresenter;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.view.RegistrationView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class RegistrationFragment extends BaseLoginFragment implements RegistrationView{

    @BindView(R.id.email_fragment_registration_edittext)
    EditText email;
    @BindView(R.id.username_fragment_registration_edittext)
    EditText username;
    @BindView(R.id.password_fragment_registration_edittext)
    EditText password;
    @BindView(R.id.repassword_fragment_registration_edittext)
    EditText repassword;
    @BindView(R.id.register_button)
    Button registrationButton;
    @BindView(R.id.register_progressbar)
    ProgressBar pBar;
    @BindView(R.id.register_constraintlayout)
    ConstraintLayout registrationLayout;
    @BindView(R.id.email_fragment_registration_inputlayout)
    TextInputLayout emailRegistrationInputlayout;
    @BindView(R.id.password_fragment_registration_inputlayout)
    TextInputLayout passwordRegistrationInputlayout;
    @BindView(R.id.repassword_fragment_registration_inputlayout)
    TextInputLayout repasswordRegistrationInputlayout;


    @Inject
    RegistrationPresenter presenter;

    @Override
    protected int setLayout() {
        return R.layout.fragment_registration;
    }

    @Override
    protected void init(View view) {

    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onSuccess(String activate_message) {
        mListener.showSuccessfullSnackbar(activate_message);
        activityUtils.removeFragmentFromActivity(fragmentManager, this);
        activityUtils.addFragmentToActivity(fragmentManager, new LoginFragment(), R.id.content_login);
    }

    @Override
    public void setEmailError() {
        emailRegistrationInputlayout.setError("Email nije valjan");
    }

    @Override
    public void setMismatchPasswordError() {
        passwordRegistrationInputlayout.setError("Lozinke se moraju slagati");
        repasswordRegistrationInputlayout.setError("Lozinke se moraju slagati");
    }

    @Override
    public void setPasswordError() {
        passwordRegistrationInputlayout.setError("Lozinka ne smije biti prazna");
    }

    @Override
    public void showLoading() {
        setEnabled(false);
        registrationButton.setVisibility(View.INVISIBLE);
        pBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        setEnabled(true);
        registrationButton.setVisibility(View.VISIBLE);
        pBar.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.open_login_fragment_textview)
    public void onLoginClickListener(){
        presenter.registerUser(username.getText().toString(), password.getText().toString(),
                repassword.getText().toString(), email.getText().toString());
    }

    @OnClick(R.id.register_constraintlayout)
    public void registerUserClickListener(){
        presenter.registerUser(username.getText().toString(),
                password.getText().toString(), repassword.getText().toString(), email.getText().toString());
    }

    private void setEnabled(boolean enabled){
        email.setEnabled(enabled);
        username.setEnabled(enabled);
        password.setEnabled(enabled);
        repassword.setEnabled(enabled);
        registrationLayout.setEnabled(enabled);
    }

}
