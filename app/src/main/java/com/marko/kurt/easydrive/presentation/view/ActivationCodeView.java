package com.marko.kurt.easydrive.presentation.view;

/**
 * Created by user1 on 2.9.2017..
 */

public interface ActivationCodeView extends LoadDataView {

    void onSuccess(String key);

    void onActivationCodeError();

}
