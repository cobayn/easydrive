package com.marko.kurt.easydrive.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by nsoft on 02/07/2017.
 */
@Entity(tableName = "event")
public class EventEntity {

    @PrimaryKey(autoGenerate = false)
    private short id;
    @ColumnInfo(name = "lat") private double lat;
    @ColumnInfo(name = "lng") private double lng;
    @ColumnInfo(name = "description") private String description;
    @ColumnInfo(name = "event_type") private short eventType;

    @Ignore
    private List<GroupEventsEntity> groupEvents;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public List<GroupEventsEntity> getGroupEvents() {
        return groupEvents;
    }

    public void setGroupEvents(List<GroupEventsEntity> groupEvents) {
        this.groupEvents = groupEvents;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }
}
