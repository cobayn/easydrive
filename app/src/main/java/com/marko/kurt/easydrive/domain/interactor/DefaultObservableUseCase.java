package com.marko.kurt.easydrive.domain.interactor;

import com.marko.kurt.easydrive.domain.interactor.type.UseCaseWithParameter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user1 on 7.12.2017..
 */

public abstract class DefaultObservableUseCase <P, R> implements UseCaseWithParameter<P, R> {

    private CompositeDisposable mCompositeDisposable;

    public DefaultObservableUseCase(CompositeDisposable compositeDisposable){
        this.mCompositeDisposable = compositeDisposable;
    }

    public abstract Observable<P> buildSingleUseCase(R param);

    @Override
    public void execute(DisposableObserver<P> observer, R param) {
        Observable<P> observable = this.buildSingleUseCase(param)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        addSingle(observable.subscribeWith(observer));
    }

    public void dispose(){

        if (!mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();

    }

    protected void addSingle(Disposable disposable){

        mCompositeDisposable.add(disposable);

    }

}
