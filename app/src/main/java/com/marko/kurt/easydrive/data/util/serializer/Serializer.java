package com.marko.kurt.easydrive.data.util.serializer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by nsoft on 08/04/2017.
 */

@Singleton
public class Serializer {

    private Gson gson = new Gson();

    @Inject
    public Serializer() {}

    public String serializeObject(Object object){
        return gson.toJson(object);
    }

    public String serialize(Object object, Class clazz){
        return gson.toJson(object, clazz);
    }

    public <T> T deserialize(String string, Class<T> clazz){
        return gson.fromJson(string, clazz);
    }

    public <T> List<T> deserializeList(String string, Class<T[]> clazz) {
        T[] arr = gson.fromJson(string, clazz);
        return Arrays.asList(arr);
    }

}
