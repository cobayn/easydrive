package com.marko.kurt.easydrive.domain;

import java.util.List;

/**
 * Created by nsoft on 02/07/2017.
 */

public class Event {

    private short id;
    private double lat, lng;
    private String descritpion;
    private short eventType;
    //private List<GroupEvents> groupEvents;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    /*public List<GroupEvents> getGroupEvents() {
        return groupEvents;
    }

    public void setGroupEvents(List<GroupEvents> groupEvents) {
        this.groupEvents = groupEvents;
    }*/

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getDescritpion() {
        return descritpion;
    }

    public void setDescritpion(String descritpion) {
        this.descritpion = descritpion;
    }

    public short getEventType() {
        return eventType;
    }

    public void setEventType(short eventType) {
        this.eventType = eventType;
    }
}
