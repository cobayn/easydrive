package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;
import com.marko.kurt.easydrive.data.entity.UserEntity;
import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit2.Response;

/**
 * Created by nsoft on 02/07/2017.
 */

public class UserEntityDataMapper {

    private Serializer mSerializer;

    public UserEntityDataMapper(Serializer serializer){
        this.mSerializer = serializer;
    }

    public User transform(UserEntity userEntity){
        User user = null;
        if (userEntity != null){
            user = new User();
            user.setUsername(userEntity.getUsername());
            user.setEmail(userEntity.getEmail());
            user.setUserType(userEntity.getUserType());
            user.setId(userEntity.getId());
            user.setPassword(userEntity.getPassword());
        }
        return user;
    }

    public List<User> transform(Collection<UserEntity> userEntityCollection){
        List<User> userList = new ArrayList<>();
        for (UserEntity userEntity : userEntityCollection){
            User user = transform(userEntity);
            if (user != null){
                userList.add(user);
            }
        }
        return userList;
    }

    public User transformLoginResponse(Response<Object> responseEntity) throws NetworkConnectionException {

        if (responseEntity.isSuccessful()){

            try {

                JSONObject bodyResponse = new JSONObject(mSerializer.serializeObject(responseEntity.body())).getJSONObject("body");
                return mSerializer.deserialize(bodyResponse.toString(), User.class);

            } catch (JSONException exception){

                throw new NetworkConnectionException(exception);

            }

        } else {

            try {

                ServerLoginResponseEntity se = mSerializer.deserialize(responseEntity.errorBody().string(), ServerLoginResponseEntity.class);
                throw new NetworkConnectionException(new Throwable(se.getMessage()));

            } catch (IOException exception) {

                throw new NetworkConnectionException(exception);

            }

        }

    }

}
