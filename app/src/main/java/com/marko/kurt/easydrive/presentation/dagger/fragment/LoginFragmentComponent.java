package com.marko.kurt.easydrive.presentation.dagger.fragment;

import com.marko.kurt.easydrive.presentation.dagger.fragment.module.LoginFragmentModule;
import com.marko.kurt.easydrive.presentation.view.fragment.LoginFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 19.9.2017..
 */
@Subcomponent(modules = LoginFragmentModule.class)
public interface LoginFragmentComponent extends AndroidInjector<LoginFragment>{

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoginFragment>{}

}
