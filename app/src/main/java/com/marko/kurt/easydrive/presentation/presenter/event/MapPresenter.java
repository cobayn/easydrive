package com.marko.kurt.easydrive.presentation.presenter.event;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by nsoft on 31/08/2017.
 */

public interface MapPresenter extends ScopedPresenter {

    void getEvents(double lat, double lng);

    void createEvent(double lat, double lng, short eventType, String description);

    void validateUser();

    void deactivateCode();

}
