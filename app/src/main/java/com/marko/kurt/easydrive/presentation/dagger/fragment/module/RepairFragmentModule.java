package com.marko.kurt.easydrive.presentation.dagger.fragment.module;

import com.marko.kurt.easydrive.presentation.presenter.login.RepairPasswordPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.RepairPasswordPresenterImpl;
import com.marko.kurt.easydrive.presentation.view.RepairFragmentView;
import com.marko.kurt.easydrive.presentation.view.fragment.RepairFragment;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by user1 on 21.9.2017..
 */
@Module
public class RepairFragmentModule {

    @Provides
    RepairFragmentView provideRepairFragmentView(RepairFragment repairFragment){
        return repairFragment;
    }

    @Provides
    RepairPasswordPresenter provideRepairPresenter(RepairFragment repairFragment){
        return new RepairPasswordPresenterImpl(repairFragment);
    }

}
