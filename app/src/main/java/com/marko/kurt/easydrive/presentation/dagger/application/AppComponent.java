package com.marko.kurt.easydrive.presentation.dagger.application;

import android.app.Application;
import android.content.BroadcastReceiver;

import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcaseReceiver;
import com.marko.kurt.easydrive.presentation.dagger.application.module.ActivityBuilder;
import com.marko.kurt.easydrive.presentation.dagger.application.module.AppModule;
import com.marko.kurt.easydrive.presentation.dagger.application.module.BroadcastReceiverBuilder;
import com.marko.kurt.easydrive.presentation.dagger.application.module.DaoModule;
import com.marko.kurt.easydrive.presentation.dagger.application.module.MapperModule;
import com.marko.kurt.easydrive.presentation.dagger.application.module.ThreadingModule;
import com.marko.kurt.easydrive.presentation.dagger.application.module.UseCaseModule;
import com.marko.kurt.easydrive.presentation.dagger.application.module.UtilsModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by user1 on 2.9.2017..
 */
@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        DaoModule.class,
        MapperModule.class,
        UseCaseModule.class,
        ThreadingModule.class,
        UtilsModule.class,
        BroadcastReceiverBuilder.class,
        ActivityBuilder.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

    void inject(EasyDriveApplication app);

}
