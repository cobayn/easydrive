package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.CheckActivationCodeStatusUseCase;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.LoginActivityView;

/**
 * Created by user1 on 19.8.2017..
 */

public final class LoginActivityPresenterImpl extends BasePresenter<LoginActivityView> implements LoginActivityPresenter{

    private SingleUseCase activationCodeUseCase;

    public LoginActivityPresenterImpl(LoginActivityView view,
                                      CheckActivationCodeStatusUseCase useCase) {
        super(view);
        this.activationCodeUseCase = useCase;
    }

    @Override
    public void checkStatusOfCode(){
        if (getNullableView() != null){
            getNullableView().showLoading();
        }
        activationCodeUseCase.execute(new LoginActivityObserver(), null);
    }

    private final class LoginActivityObserver extends DeafultSingleObserver<ServerLoginReponse>{

        @Override
        public void onError(Throwable e) {
            getNullableView().hideLoading();
            String errorMessage = ErrorMessageFactory.create(new DefaultErrorBundle((Exception) e).getException());
            getNullableView().showError(errorMessage);
            getNullableView().openActivationFragment();
        }

        @Override
        public void onSuccess(ServerLoginReponse serverLoginReponse) {
            getNullableView().hideLoading();
            if (serverLoginReponse.isUserRemember()){
                getNullableView().openMapActivity();
            } else {
                getNullableView().openLoginFragment();
            }
        }
    }

}
