package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by nsoft on 17/07/2017.
 */

public class ActivateCodeUsecase extends DefaultSingleUseCase<ServerLoginReponse, String>{

    private UserRepository mUserRepository;

    @Inject
    public ActivateCodeUsecase(CompositeDisposable compositeDisposable, UserRepository userRepositorz){
        super(compositeDisposable);
        this.mUserRepository = userRepositorz;
    }

    @Override
    public Single<ServerLoginReponse> buildSingleUseCase(String param) {
        return mUserRepository.activationCode(param);
    }

}
