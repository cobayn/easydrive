package com.marko.kurt.easydrive.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.marko.kurt.easydrive.data.entity.EventEntity;

import java.util.List;

/**
 * Created by user1 on 5.12.2017..
 */
@Dao
public interface EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<EventEntity> eventEntities);

    @Query("SELECT * FROM event")
    List<EventEntity> getLocalEvents();

    @Query("DELETE FROM event")
    public void deleteAllFromTable();

    @Update
    public void updateAllFromTable(List<EventEntity> eventEntities);

}
