package com.marko.kurt.easydrive.presentation.dagger.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.marko.kurt.easydrive.presentation.dagger.ComponentFactory;
import com.marko.kurt.easydrive.presentation.dagger.application.EasyDriveApplication;

import dagger.android.AndroidInjection;

/**
 * Created by user1 on 12.8.2017..
 */

public abstract class DaggerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

}
