package com.marko.kurt.easydrive.presentation.view.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.presenter.login.RepairPasswordPresenter;
import com.marko.kurt.easydrive.presentation.presenter.login.RepairPasswordPresenterImpl;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.view.RepairFragmentView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class RepairFragment extends BaseFragment implements RepairFragmentView{

    @BindView(R.id.repair_password_edittext)
    EditText repairPasswordEdittext;
    @BindView(R.id.repair_password_button)
    Button repairPasswordButton;

    @Inject
    RepairPasswordPresenter presenter;

    @Override
    protected int setLayout() {
        return R.layout.fragment_repair;
    }

    @Override
    protected void init(View view) {
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showMessage(String message) {
        toastMessage(message);
    }

    private void setEnabled(boolean enabled){
        repairPasswordButton.setEnabled(enabled);
        repairPasswordEdittext.setEnabled(enabled);
    }

    @Override
    public void showLoading() {
        setEnabled(false);
    }

    @Override
    public void hideLoading() {
        setEnabled(true);
    }

    @Override
    public void showError(String error) {

    }

    @OnClick(R.id.repair_password_button)
    public void onRepairClickListener(){
        presenter.repairPassword(repairPasswordEdittext.getText().toString());
    }

}
