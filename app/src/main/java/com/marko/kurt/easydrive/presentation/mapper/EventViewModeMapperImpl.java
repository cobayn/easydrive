package com.marko.kurt.easydrive.presentation.mapper;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.presentation.model.EventModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 18.8.2017..
 */

public class EventViewModeMapperImpl implements EventViewModeMapper {

    @Override
    public EventModel mapEventToViewModel(Event event) {
        EventModel eventModel = null;
        if (event != null){
            eventModel = new EventModel();
            eventModel.setId(event.getId());
            eventModel.setLat(event.getLat());
            eventModel.setLng(event.getLng());
            eventModel.setDescription(event.getDescritpion());
            eventModel.setEventType(event.getEventType());
        }
        return eventModel;
    }

    @Override
    public List<EventModel> mapCollectionsToViewModel(List<Event> eventList) {
        List<EventModel> eventModelList = new ArrayList<>();
        for (Event event : eventList){
            if (event != null){
                eventModelList.add(mapEventToViewModel(event));
            }
        }
        return eventModelList;
    }
}
