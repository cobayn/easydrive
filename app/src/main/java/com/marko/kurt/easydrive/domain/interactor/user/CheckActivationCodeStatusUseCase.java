package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by kurt on 14/11/2017.
 */

public class CheckActivationCodeStatusUseCase extends DefaultSingleUseCase<ServerLoginReponse, Void> {

    private UserRepository mUserRepository;

    @Inject
    public CheckActivationCodeStatusUseCase(CompositeDisposable compositeDisposable, UserRepository userRepository){
        super(compositeDisposable);
        this.mUserRepository = userRepository;
    }

    @Override
    public Single<ServerLoginReponse> buildSingleUseCase(Void param) {
        return mUserRepository.checkCodeStatus();
    }
}
