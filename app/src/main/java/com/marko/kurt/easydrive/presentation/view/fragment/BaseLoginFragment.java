package com.marko.kurt.easydrive.presentation.view.fragment;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.marko.kurt.easydrive.presentation.util.ActivityUtils;
import com.marko.kurt.easydrive.presentation.util.OnControlSnackbarListener;
import com.marko.kurt.easydrive.presentation.view.LoadDataView;

import javax.inject.Inject;

/**
 * Created by user1 on 31.10.2017..
 */

public abstract class BaseLoginFragment extends BaseFragment implements LoadDataView {

    protected OnControlSnackbarListener mListener;

    @Inject
    ActivityUtils activityUtils;
    @Inject
    FragmentManager fragmentManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnControlSnackbarListener){
            mListener = (OnControlSnackbarListener) context;
        } else {
            throw new ClassCastException("implement interface");
        }
    }

    @Override
    public void showError(String error) {
        mListener.showErrorSnackbar(error);
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }
}
