package com.marko.kurt.easydrive.domain.interactor.type;

import com.marko.kurt.easydrive.domain.User;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by kurt on 19/11/2017.
 */

public interface LoginSingleUsecase<P, R> extends SingleUseCase<P, R> {

    void executeLoginUser(DisposableSingleObserver<User> observer, String[] credentials, boolean remember_me);

}
