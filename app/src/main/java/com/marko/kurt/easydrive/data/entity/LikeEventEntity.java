package com.marko.kurt.easydrive.data.entity;

/**
 * Created by kurt on 21/11/2017.
 */

public class LikeEventEntity {

    private short eventGroupId;
    private short rate;

    public LikeEventEntity() {}

    public LikeEventEntity(short eventGroupId, short rate){
        this.eventGroupId = eventGroupId;
        this.rate = rate;
    }

    public short getEventGroupId() {
        return eventGroupId;
    }

    public void setEventGroupId(short eventGroupId) {
        this.eventGroupId = eventGroupId;
    }

    public short getRate() {
        return rate;
    }

    public void setRate(short rate) {
        this.rate = rate;
    }
}
