package com.marko.kurt.easydrive.presentation.mapper;

import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.presentation.model.UserModel;

import java.util.Collection;

/**
 * Created by kurt on 14/11/2017.
 */

public interface UserViewModelMapper {

    public UserModel transform(User user);

    public Collection<UserModel> transform(Collection<User> userList);

}
