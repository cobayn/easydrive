package com.marko.kurt.easydrive.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.marko.kurt.easydrive.data.entity.UserEntity;

/**
 * Created by user1 on 9.12.2017..
 */
@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(UserEntity userEntity);

    @Query("SELECT * FROM user")
    UserEntity getUserFromDatabase();

    @Query("DELETE FROM user")
    void deleteUser();

}