package com.marko.kurt.easydrive.domain.interactor;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by nsoft on 01/07/2017.
 */

public abstract class DefaultObserver<T> extends DisposableObserver<T> {

    @Override
    public void onNext(T t) {

    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
