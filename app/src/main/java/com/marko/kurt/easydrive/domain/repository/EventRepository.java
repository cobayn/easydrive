package com.marko.kurt.easydrive.domain.repository;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.User;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by nsoft on 02/07/2017.
 */

public interface EventRepository {

    Observable<Event> getEvents(double lat, double lng);

    Single<ServerLoginReponse> likeEvents(short groupEventId, short rate);

    Single<ServerLoginReponse> dislikeEvent(short groupEventId, short rate);

    Single<ServerLoginReponse> createEvent(double lat, double lng, short eventType, String description);

    void deactivateUserCode();

}
