package com.marko.kurt.easydrive.presentation.util;

import android.app.Notification;

/**
 * Created by user1 on 2.12.2017..
 */

public interface Notifications {

    void showNotification(int notificationId, Notification notification);

    void updateNotification(int notificationId, Notification notification);

    void hideNotification(int notificationId);

    void hideNotifications();

}
