package com.marko.kurt.easydrive.presentation.dagger.fragment;

import javax.inject.Scope;

/**
 * Created by user1 on 19.8.2017..
 */

@Scope
public @interface FragmentScope {
}
