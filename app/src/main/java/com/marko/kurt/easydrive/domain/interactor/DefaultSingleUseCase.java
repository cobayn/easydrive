package com.marko.kurt.easydrive.domain.interactor;

import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kurt on 14/11/2017.
 */

public abstract class DefaultSingleUseCase<P, R> implements SingleUseCase<P, R>{

    private CompositeDisposable mCompositeDisposable;

    public DefaultSingleUseCase(CompositeDisposable compositeDisposable){
        this.mCompositeDisposable = compositeDisposable;
    }

    public abstract Single<P> buildSingleUseCase(R param);

    @Override
    public void execute(DisposableSingleObserver<P> observer, R param) {
        Single<P> observable = this.buildSingleUseCase(param)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        addSingle(observable.subscribeWith(observer));
    }

    public void dispose(){

        if (!mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();

    }

    protected void addSingle(Disposable disposable){

        mCompositeDisposable.add(disposable);

    }

}
