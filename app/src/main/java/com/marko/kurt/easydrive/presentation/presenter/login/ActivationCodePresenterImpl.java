package com.marko.kurt.easydrive.presentation.presenter.login;

import android.util.Log;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.exception.DefaultErrorBundle;
import com.marko.kurt.easydrive.domain.interactor.DeafultSingleObserver;
import com.marko.kurt.easydrive.domain.interactor.type.SingleUseCase;
import com.marko.kurt.easydrive.domain.interactor.user.ActivateCodeUsecase;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;
import com.marko.kurt.easydrive.presentation.presenter.BasePresenter;
import com.marko.kurt.easydrive.presentation.view.ActivationCodeView;

/**
 * Created by nsoft on 07/09/2017.
 */

public class ActivationCodePresenterImpl extends BasePresenter<ActivationCodeView> implements ActivationCodePresenter {

    private SingleUseCase mLoginUsecase;

    public ActivationCodePresenterImpl(ActivationCodeView view, ActivateCodeUsecase activateCodeUsecase) {
        super(view);
        this.mLoginUsecase = activateCodeUsecase;
    }

    @Override
    public void validateCode(String code) {

        if (code.isEmpty()){
            getNullableView().onActivationCodeError();
            return;
        }
        getNullableView().showLoading();
        Log.i("Ovo je code", code);

        mLoginUsecase.execute(new ActivationCodeObserver(), code);

    }

    private class ActivationCodeObserver extends DeafultSingleObserver<ServerLoginReponse>{

        @Override
        public void onSuccess(ServerLoginReponse serverLoginReponse) {
            getNullableView().hideLoading();
            getNullableView().onSuccess(serverLoginReponse.getMessage());
        }

        @Override
        public void onError(Throwable e) {
            getNullableView().hideLoading();
            String errorMessage = ErrorMessageFactory.create(new DefaultErrorBundle((Exception) e).getException());
            getNullableView().showError(errorMessage);
        }
    }

}