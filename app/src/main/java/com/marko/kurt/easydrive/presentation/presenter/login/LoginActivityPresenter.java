package com.marko.kurt.easydrive.presentation.presenter.login;

import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;

/**
 * Created by user1 on 4.11.2017..
 */

public interface LoginActivityPresenter extends ScopedPresenter {

    void checkStatusOfCode();

}
