package com.marko.kurt.easydrive.data.util.connectivity;

/**
 * Created by user1 on 19.8.2017..
 */

public final class NetworkData {

    public final boolean hasInternetConnection;
    public final boolean isMobileConnection;

    public NetworkData(final boolean hasInternetConnection, final boolean isMobileConnection){
        this.hasInternetConnection = hasInternetConnection;
        this.isMobileConnection = isMobileConnection;
    }

}
