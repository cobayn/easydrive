package com.marko.kurt.easydrive.data.repository;

import android.location.Location;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.marko.kurt.easydrive.data.db.dao.EventDao;
import com.marko.kurt.easydrive.data.db.dao.UserDao;
import com.marko.kurt.easydrive.data.entity.CreateEventEntity;
import com.marko.kurt.easydrive.data.entity.EventEntity;
import com.marko.kurt.easydrive.data.entity.LikeEventEntity;
import com.marko.kurt.easydrive.data.entity.mapper.EventEntityDataMapper;
import com.marko.kurt.easydrive.data.entity.mapper.GroupEventsEntityDataMapper;
import com.marko.kurt.easydrive.data.entity.mapper.ServerLoginReponseDataMapper;
import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.data.net.EventApi;
import com.marko.kurt.easydrive.data.util.AppUtils;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.data.util.TelephonIdGenerator;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapper;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.util.GeoPoint;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import retrofit2.Response;

/**
 * Created by nsoft on 02/07/2017.
 */

public class EventRepositoryImpl implements EventRepository {

    private EventApi eventApi;
    private EventEntityDataMapper eventEntityDataMapper;
    private ServerLoginReponseDataMapper serverReponseMapper;
    private Serializer mSerializer;
    private FileManager mFIleManager;
    private TelephonIdGenerator telephoneGenerator;
    private ConnectivityManagerWrapper connectionManagerWrapper;
    private EventDao mEventDao;
    private RoadManager mRoadManager;

    public EventRepositoryImpl(EventApi eventApi, FileManager fileManager, TelephonIdGenerator generator,
                               ConnectivityManagerWrapper connectivityManagerWrapper, EventDao eventDao,
                               RoadManager roadManager){
        this.eventApi = eventApi;
        this.mFIleManager = fileManager;
        this.telephoneGenerator = generator;
        this.connectionManagerWrapper = connectivityManagerWrapper;
        this.mEventDao = eventDao;
        this.mRoadManager = roadManager;
        serverReponseMapper = new ServerLoginReponseDataMapper(mSerializer);
        eventEntityDataMapper = new EventEntityDataMapper(new Serializer());
    }

    @Override
    public Observable<Event> getEvents(double lat, double lng) {

        Location myLocation = new Location("");
        myLocation.setLatitude(lat);
        myLocation.setLongitude(lng);

        if (connectionManagerWrapper.isConnectedToNetwork()) {

            try {


                return getRemoteEvents(lat, lng)
                        .map(evententities -> eventEntityDataMapper.transform(evententities))
                        .flatMap(events -> Observable.fromIterable(events)) /*new Function<List<Event>, ObservableSource<Event>>() {
                    @Override
                    public ObservableSource<Event> apply(List<Event> events) throws Exception {
                        Log.i("usao u observable", "u observbleu sam");
                        return Observable.fromIterable(events);
                    }})*/
                        .filter(event -> ((int)getDistance(lat, lng, event.getLat(), event.getLng()) < 5))/*new Predicate<Event>() {
                              @Override
                              public boolean test(Event event) throws Exception {
                                  int a = (int) getDistance(lat, lng, event.getLat(), event.getLng());
                                  return ((int)getDistance(lat, lng, event.getLat(), event.getLng()) < 5);
                              }
                          }
                        )*/
                        .flatMap(event -> Observable.just(event));/*new Function<Event, ObservableSource<Event>>() {
                    @Override
                    public ObservableSource<Event> apply(Event event) throws Exception {
                        return Observable.just(event);
                    }});*/

            } catch (MalformedURLException exception){

                if (mEventDao.getLocalEvents().size() > 0){

                    return Observable.just(mEventDao.getLocalEvents()).map(eventEntities -> eventEntityDataMapper.transform(eventEntities))
                            .flatMap(new Function<List<Event>, ObservableSource<Event>>() {
                                @Override
                                public ObservableSource<Event> apply(List<Event> events) throws Exception {
                                    return Observable.fromIterable(events);
                                }
                            })
                            .filter(new Predicate<Event>() {
                                @Override
                                public boolean test(Event events) throws Exception {
                                    Location eventLocation = new Location("");
                                    eventLocation.setLatitude(events.getLat());
                                    eventLocation.setLongitude(events.getLng());
                                    int a = (int) eventLocation.distanceTo(myLocation);
                                    return ((int)eventLocation.distanceTo(myLocation) < 5);
                                }
                            }).flatMap(new Function<Event, ObservableSource<Event>>() {
                                @Override
                                public ObservableSource<Event> apply(Event event) throws Exception {
                                    return Observable.just(event);
                                }
                            });

                } else {

                    return Observable.create(emitter -> {

                        emitter.onError(new NetworkConnectionException(exception));

                    });

                }

            }


        } else {


            if (mEventDao.getLocalEvents().size() > 0){
                return Observable.just(mEventDao.getLocalEvents()).map(eventEntities -> eventEntityDataMapper.transform(eventEntities))
                        .flatMap(new Function<List<Event>, ObservableSource<Event>>() {
                            @Override
                            public ObservableSource<Event> apply(List<Event> events) throws Exception {
                                return Observable.fromIterable(events);
                            }
                        })
                        .filter(new Predicate<Event>() {
                            @Override
                            public boolean test(Event events) throws Exception {
                                Location eventLocation = new Location("");
                                eventLocation.setLatitude(events.getLat());
                                eventLocation.setLongitude(events.getLng());
                                int a = (int) eventLocation.distanceTo(myLocation);
                                return ((int)eventLocation.distanceTo(myLocation) < 5);
                            }
                        }).flatMap(new Function<Event, ObservableSource<Event>>() {
                            @Override
                            public ObservableSource<Event> apply(Event event) throws Exception {
                                return Observable.just(event);
                            }
                        });
            } else {
                return Observable.create(emiiter -> {
                    emiiter.onError(new NetworkConnectionException());
                });
            }
        }
    }

    @Override
    public Single<ServerLoginReponse> likeEvents(short groupEventId, short rate) {
        if (connectionManagerWrapper.isConnectedToNetwork()) {
            return eventApi.likeEvent(getHeaders(), new LikeEventEntity(groupEventId, rate))
                    .map(likedEvents -> serverReponseMapper.transformResponse(likedEvents)
                    );
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public Single<ServerLoginReponse> dislikeEvent(short groupEventId, short rate) {
        if (connectionManagerWrapper.isConnectedToNetwork()) {
            return eventApi.dislikeEvent(getHeaders(), new LikeEventEntity(groupEventId, rate))
                    .map(dislikeEvents -> serverReponseMapper.transformResponse(dislikeEvents)
                    );
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public Single<ServerLoginReponse> createEvent(double lat, double lng, short eventType, String description) {
        if (connectionManagerWrapper.isConnectedToNetwork()) {
            return eventApi.createEvent(getHeaders(), new CreateEventEntity(lat, lng, eventType, description))
                    .map(createdEvent -> serverReponseMapper.transformResponse(createdEvent));
        } else {
            return Single.create(emitter -> emitter.onError(new NetworkConnectionException()));
        }
    }

    @Override
    public void deactivateUserCode() {
        mFIleManager.writeToPreference(AppUtils.APPLICATION_MODE_KEY, null);
        mFIleManager.writeBooleanToPreference(AppUtils.REMEBER_USER, false);
        mFIleManager.writeToPreference(AppUtils.EMAIL, null);
        mFIleManager.writeIntToPreference(AppUtils.USER_TYPE, 0);
    }

    @VisibleForTesting
    HashMap<String, String> getHeaders(){

        HashMap<String, String> headers = new HashMap<>();
        headers.put(AppUtils.HEADER_REQUEST_CONTENT_TYPE, AppUtils.HEADER_CONTENT_TYPE);
        headers.put(AppUtils.HEADER_AUTHENTICATION_CODE, mFIleManager.getFromPreferences(AppUtils.APPLICATION_MODE_KEY));
        headers.put(AppUtils.HEADER_DEVICE_ID, telephoneGenerator.newId());
        headers.put(AppUtils.SESSION_ID, mFIleManager.getFromPreferences(AppUtils.SESSION_ID));
        return headers;

    }

    @VisibleForTesting
    HashMap<String, Double> getQueryParams(double lat, double lng){

        HashMap<String, Double> queries = new HashMap<>();
        queries.put(AppUtils.PARAM_LAT, lat);
        queries.put(AppUtils.PARAM_LNG, lng);
        return queries;

    }

    private Observable<List<EventEntity>> getRemoteEvents(double lat, double lng) throws MalformedURLException {

        return eventApi.getAllEvents(getQueryParams(lat, lng), getHeaders())
                .map(objectList -> eventEntityDataMapper.transformFromObjectList(objectList))
                .doOnNext(events -> {
                    for (EventEntity eventEntity : events)
                        Log.i("ovo je event", eventEntity.getDescription());
                    mEventDao.updateAllFromTable(events);
                });

    }

    private double getDistance(double mylat, double mylng, double eventlat, double eventlng){

        ArrayList<GeoPoint> waypoint = new ArrayList<>();
        waypoint.add(new GeoPoint(mylat,mylng));
        waypoint.add(new GeoPoint(eventlat,eventlng));
        Road road = mRoadManager.getRoad(waypoint);
        return road.mLength;

    }

}