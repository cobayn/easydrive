package com.marko.kurt.easydrive.data.exception;

/**
 * Created by user1 on 5.11.2017..
 */

public class NetworkConnectionException extends Exception {

    public NetworkConnectionException(){
        super();
    }

    public NetworkConnectionException(final Throwable cause){
        super(cause);
    }

}
