package com.marko.kurt.easydrive.presentation.view

/**
 * Created by user1 on 4.11.2017..
 */
interface LoginActivityView : LoadDataView {

    fun openActivationFragment()

    fun openLoginFragment()

    fun finishApplicationWork()

    fun openMapActivity();

}