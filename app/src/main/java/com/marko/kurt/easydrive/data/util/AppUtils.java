package com.marko.kurt.easydrive.data.util;

/**
 * Created by nsoft on 06/09/2017.
 */

public class AppUtils {

    //request headers
    public static final String HEADER_REQUEST_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_CONTENT_TYPE = "application/json";
    public static final String HEADER_DEVICE_ID = "device_id";
    public static final String HEADER_AUTHENTICATION_CODE = "activation_code";

    //preferences
    public static final String APPLICATION_MODE_KEY = "application_key";
    public static final String EMAIL = "email";
    public static final String USER_TYPE = "user_type";
    public static final String REMEBER_USER = "remember_user";
    public static final String SESSION_ID = "session_id";

    //request params
    public static final String PARAM_LAT = "lat";
    public static final String PARAM_LNG = "lng";

}
