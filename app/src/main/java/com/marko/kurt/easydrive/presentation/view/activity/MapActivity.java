package com.marko.kurt.easydrive.presentation.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.marko.kurt.easydrive.R;
import com.marko.kurt.easydrive.presentation.broadcaseReceiver.LikeBroadcaseReceiver;
import com.marko.kurt.easydrive.presentation.model.EventModel;
import com.marko.kurt.easydrive.presentation.model.UserModel;
import com.marko.kurt.easydrive.presentation.presenter.ScopedPresenter;
import com.marko.kurt.easydrive.presentation.presenter.event.MapPresenter;
import com.marko.kurt.easydrive.presentation.util.CreateEventListener;
import com.marko.kurt.easydrive.presentation.util.NotificationFactory;
import com.marko.kurt.easydrive.presentation.util.Notifications;
import com.marko.kurt.easydrive.presentation.view.dialog.CreateEventDialog;
import com.marko.kurt.easydrive.presentation.view.service.LocationUpdateService;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.DirectedLocationOverlay;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasBroadcastReceiverInjector;

public class MapActivity extends BaseActivity implements com.marko.kurt.easydrive.presentation.view.MapView, CreateEventListener,
NavigationView.OnNavigationItemSelectedListener, MapEventsReceiver{

    private static final int LOCATION_REQUEST = 1111;
    private static final int NEW_ARTICLES_NOTIFICATION_ID = 1832;
    private static final String DIALOG_TAG = "create_dialog";

    private DirectedLocationOverlay overlay;
    private GoogleApiClient googleApiClient;
    private Location mLocation;
    private IMapController mapController;
    private LikeBroadcaseReceiver likeBroadcastReceiver = new LikeBroadcaseReceiver();

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.map)
    MapView map;

    @Inject
    MapPresenter presenter;

    @Inject
    Notifications notifications;

    @Inject
    NotificationFactory notificationFactory;

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError(String error) {
        Log.i("oco je error", error);
    }

    @Override
    protected int setLayout() {
        return R.layout.activity_map;
    }

    @Override
    protected void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.map_toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toogle);
        toogle.syncState();

        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mLocationReceiver, new IntentFilter("location"));
        super.onResume();
        permissionPresenter.requestLocationStoragePermission();
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected View getSnackbarView() {
        return findViewById(R.id.map_coordinatorlayout);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @OnClick(R.id.create_event_fab)
    public void createEventListener(){
        CreateEventDialog dialog = new CreateEventDialog();
        dialog.show(fragmentManager, DIALOG_TAG);
    }

    private void displayLocationSettingsRequest(){

        if (googleApiClient == null)
            googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).build();
        googleApiClient.connect();


        LocationRequest locationRequest = new LocationRequest();

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates states = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()){

                    case LocationSettingsStatusCodes.SUCCESS:
                        startService(new Intent(MapActivity.this, LocationUpdateService.class));
                        showCurrentLocation();
                        presenter.validateUser();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MapActivity.this, LOCATION_REQUEST);
                        } catch (IntentSender.SendIntentException e){
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("Nemoj pokazivat dialog", "Nemoj pokazivat dialog");
                        break;

                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_REQUEST){
            startService(new Intent(MapActivity.this, LocationUpdateService.class));
            showCurrentLocation();
            presenter.validateUser();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void permissionAccepted(int actionCode) {
        displayLocationSettingsRequest();
    }

    @Override
    public void showEvents(List<EventModel> eventModelList) {

        for (EventModel eventModel : eventModelList){

            /*Location location = new Location("");
            //location.setLatitude(eventModel.getGroupEventsModelList().get(0).getLat());
            //location.setLongitude(eventModel.getGroupEventsModelList().get(0).getLng());
            int distanceValue = (int)location.distanceTo(mLocation);
            Log.i("ovo je distance", String.valueOf(distanceValue));
            if (location.distanceTo(mLocation) < 10){
                Log.i("show warning", "show warning");
            }*/

            Marker eventLocation = new Marker(map);
            eventLocation.setPosition(new GeoPoint(eventModel.getLat(), eventModel.getLng()));
            eventLocation.setIcon(getResources().getDrawable(R.drawable.marker_default));
            map.getOverlays().add(eventLocation);


        }
        map.invalidate();

    }

    /**
     * if user is not admin or moderator, then his location is always centered
     */

    @Override
    public void setCenteredUser() {
        if (mLocation != null) {
            mapController.animateTo(new GeoPoint(mLocation.getLatitude(), mLocation.getLongitude()));
            map.invalidate();
        }
    }

    /**
     * if user is admin or moderator, he can add events on click on map
     */

    @Override
    public void alowUserToSeeRestOfMap() {

        MapEventsOverlay mapEventsOverlay = new MapEventsOverlay(this, this);
        map.getOverlays().add(0, mapEventsOverlay);

    }

    @Override
    public void setUserInformactionsToNavigationView(UserModel userModel) {
        TextView header_email = navigationView.findViewById(R.id.user_name_nav_header_textview);
        header_email.setText(userModel.getEmail());
        TextView avatar_image = navigationView.findViewById(R.id.user_profile_imageview);
        avatar_image.setText(userModel.getEmail().substring(0, 1));
    }

    @Override
    public void showNotification() {
        notifications.showNotification(NEW_ARTICLES_NOTIFICATION_ID, notificationFactory.createNewNotification(
                "police", true, true, true
        ));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("like");
        intentFilter.addAction("dislike");
        registerReceiver(likeBroadcastReceiver, intentFilter);
    }

    private void showCurrentLocation() {

        map.setTileSource(TileSourceFactory.MAPNIK);

        mapController = map.getController();
        mapController.setZoom(8);
        mapController.setCenter(new GeoPoint(44.207353, 17.789688));

        overlay = new DirectedLocationOverlay(this);
        overlay.setShowAccuracy(true);
        map.getOverlays().add(overlay);

        RotationGestureOverlay rotationGestureOverlay = new RotationGestureOverlay(this, map);
        rotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(rotationGestureOverlay);

    }

    private BroadcastReceiver mLocationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mLocation = intent.getParcelableExtra("currentLocation");
            overlay.setBearing(mLocation.getBearing());
            overlay.setAccuracy((int)mLocation.getAccuracy());
            overlay.setLocation(new GeoPoint(mLocation.getLatitude(), mLocation.getLongitude()));
            presenter.getEvents(mLocation.getLatitude(), mLocation.getLongitude());
        }
    };

    @Override
    public void createEventListener(short type) {
        presenter.createEvent(mLocation.getLatitude(), mLocation.getLongitude(), type, "bezvezni description");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.nav_settings:
                drawerLayout.closeDrawer(GravityCompat.START);
                router.openSettingsActivity();
                break;
            case R.id.nav_profile:
                break;
            case R.id.deactivate_code:
                this.finish();
                presenter.deactivateCode();
                break;
            default:
                Log.i("Ovo je default", "default");

        }
        return true;
    }

    @Override
    protected void onDestroy() {
        notifications.hideNotification(NEW_ARTICLES_NOTIFICATION_ID);
        unregisterReceiver(likeBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocationReceiver);
        super.onDestroy();
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        return true;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        CreateEventDialog dialog = new CreateEventDialog();
        dialog.show(fragmentManager, DIALOG_TAG);
        return true;
    }
}