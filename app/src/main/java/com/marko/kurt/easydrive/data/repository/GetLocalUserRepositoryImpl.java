package com.marko.kurt.easydrive.data.repository;

import com.marko.kurt.easydrive.data.util.AppUtils;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.domain.User;
import com.marko.kurt.easydrive.domain.repository.LocalUserRepository;

import io.reactivex.Single;

/**
 * Created by user1 on 9.12.2017..
 */

public class GetLocalUserRepositoryImpl implements LocalUserRepository {

    private FileManager mFileManager;

    public GetLocalUserRepositoryImpl(FileManager fileManager){
        this.mFileManager = fileManager;
    }

    @Override
    public Single<User> getLocalUser() {
        return Single.just(new User(mFileManager.getIntFromPreferences(AppUtils.USER_TYPE),
                mFileManager.getFromPreferences(AppUtils.EMAIL)));
    }
}
