package com.marko.kurt.easydrive.presentation.broadcaseReceiver;

import com.marko.kurt.easydrive.presentation.view.LoadDataView;

/**
 * Created by nsoft on 19/12/2017.
 */

public interface LikeBroadcastReceiverView extends LoadDataView{

    void showSuccesfullMessage(String message);

}
