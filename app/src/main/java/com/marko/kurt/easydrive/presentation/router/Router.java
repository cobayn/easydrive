package com.marko.kurt.easydrive.presentation.router;

/**
 * Created by nsoft on 21/08/2017.
 */

public interface Router {

    void closeScreen();

    void goBack();

    void startMapActivity();

    void openActivationFragment();

    void openRegistrationFragment();

    void openLoginFragment();

    void openRepairPasswordFragment();

    void openSettingsActivity();

    void openPermissionSettingsActivity();

    void openInfoActivity();

}
