package com.marko.kurt.easydrive.presentation.dagger.fragment;

import com.marko.kurt.easydrive.presentation.dagger.fragment.module.RegistrationFragmentModule;
import com.marko.kurt.easydrive.presentation.view.fragment.RegistrationFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by user1 on 21.9.2017..
 */

@Subcomponent(modules = RegistrationFragmentModule.class)
public abstract interface RegistrationFragmentComponent extends AndroidInjector<RegistrationFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RegistrationFragment>{}

}
