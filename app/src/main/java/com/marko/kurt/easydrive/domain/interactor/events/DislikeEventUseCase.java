package com.marko.kurt.easydrive.domain.interactor.events;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by user1 on 2.12.2017..
 */

public class DislikeEventUseCase extends DefaultSingleUseCase<ServerLoginReponse, short[]> {

    private EventRepository eventRepository;

    @Inject
    public DislikeEventUseCase(CompositeDisposable compositeDisposable, EventRepository eventRepository) {
        super(compositeDisposable);
        this.eventRepository = eventRepository;
    }

    @Override
    public Single<ServerLoginReponse> buildSingleUseCase(short[] param) {
        return eventRepository.dislikeEvent(param[0], param[1]);
    }
}
