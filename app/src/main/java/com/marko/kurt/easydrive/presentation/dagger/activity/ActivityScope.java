package com.marko.kurt.easydrive.presentation.dagger.activity;

import javax.inject.Scope;

/**
 * Created by user1 on 18.8.2017..
 */
@Scope
public @interface ActivityScope {
}
