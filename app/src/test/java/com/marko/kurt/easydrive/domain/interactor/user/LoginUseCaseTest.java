package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kurt on 17/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginUseCaseTest {

    private final String TEST_USERNAME = "username";
    private final String TEST_PASSWORD = "password";
    private final boolean TEST_REMEMBER_ME = false;

    private LoginUsecase loginUsecase;
    private CompositeDisposable compositeDisposable;

    @Mock private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        loginUsecase = new LoginUsecase(compositeDisposable, userRepository);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.io();
            }
        });
    }

    @Test
    public void shouldLoginUser() throws Exception {

        final ServerLoginReponse serverLoginReponse = new ServerLoginReponse();

        Mockito.when(userRepository.loginUser(TEST_USERNAME, TEST_PASSWORD, TEST_REMEMBER_ME)).thenReturn(Single.just(serverLoginReponse));

        loginUsecase.executeLoginUser(new TestDisposableObserver<>(), new String[]{TEST_USERNAME, TEST_PASSWORD}, TEST_REMEMBER_ME);


        Mockito.verify(userRepository, Mockito.times(1)).loginUser(TEST_USERNAME, TEST_PASSWORD, TEST_REMEMBER_ME);
        Mockito.verifyNoMoreInteractions(userRepository);

    }

    private static class TestDisposableObserver<T> extends DisposableSingleObserver<T> {

        private int valueCOunt = 0;

        @Override
        public void onSuccess(T t) {
            valueCOunt++;
        }

        @Override
        public void onError(Throwable e) {

        }
    }

}
