package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by kurt on 17/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ActivationCodeUsecaseTest {

    private static final String TEST_ACTIVATION_CODE = "mck45ks";

    private CompositeDisposable compositeDisposable;
    private ActivateCodeUsecase activateCodeUsecase;

    @Mock private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        activateCodeUsecase = new ActivateCodeUsecase(compositeDisposable, userRepository);
    }

    @Test
    public void succesfullyActivateCode() throws Exception{

        final ServerLoginReponse serverLoginReponse = new ServerLoginReponse();

        Mockito.when(userRepository.activationCode(TEST_ACTIVATION_CODE)).thenReturn(Single.just(serverLoginReponse));

        activateCodeUsecase.buildSingleUseCase(TEST_ACTIVATION_CODE).subscribeWith(new TestDisposableObserver<>());

        Mockito.verify(userRepository, Mockito.times(1)).activationCode(TEST_ACTIVATION_CODE);
        Mockito.verifyNoMoreInteractions(userRepository);

    }

    private static class TestDisposableObserver<T> extends DisposableSingleObserver<T> {

        private int valueCOunt = 0;

        @Override
        public void onSuccess(T t) {
            valueCOunt++;
        }

        @Override
        public void onError(Throwable e) {

        }
    }

}
