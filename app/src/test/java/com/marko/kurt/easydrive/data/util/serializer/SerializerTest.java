package com.marko.kurt.easydrive.data.util.serializer;

import com.marko.kurt.easydrive.data.entity.EventEntity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by user1 on 5.9.2017..
 */
@RunWith(MockitoJUnitRunner.class)
public class SerializerTest {

    private static final String JSON_RESPONSE = "{\"id\": 1, \"description\": \"kako je stari!\"}";

    private Serializer serializer;

    @Before
    public void setUp() throws Exception {
        serializer = new Serializer();
    }

    @Test
    public void testSerializeCase() {

        final EventEntity eventEntityOne = serializer.deserialize(JSON_RESPONSE, EventEntity.class);
        final String jsonString = serializer.serialize(eventEntityOne, EventEntity.class);
        final EventEntity eventEntityTwo = serializer.deserialize(jsonString, EventEntity.class);

        assertThat(eventEntityOne.getId(), is(eventEntityTwo.getId()));
        //assertThat(eventEntityOne.getDescription(), is(eventEntityTwo.getDescription()));

    }

    @Test
    public void testDeserializeCase() {

        final EventEntity eventEntity = serializer.deserialize(JSON_RESPONSE, EventEntity.class);
        assertThat(eventEntity.getId(), is(1));

    }

}
