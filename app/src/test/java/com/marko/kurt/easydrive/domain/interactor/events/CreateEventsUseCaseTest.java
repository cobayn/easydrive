package com.marko.kurt.easydrive.domain.interactor.events;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by user1 on 2.12.2017..
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateEventsUseCaseTest {

    private final double TEST_LAT = 43.12;
    private final double TEST_LNG = 17.43;
    private final short TEST_TYPE = 1;
    private final String TEST_DESCRIPTION = "neki description";

    private CreateEventUseCase createEventusecase;
    private CompositeDisposable compositeDisposable;

    @Mock private EventRepository eventRepository;

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        createEventusecase = new CreateEventUseCase(compositeDisposable, eventRepository);
    }

    @Test
    public void shouldCreateEvent() throws Exception {

        ServerLoginReponse serverLoginReponse = new ServerLoginReponse();

        Mockito.when(eventRepository.createEvent(TEST_LAT, TEST_LNG, TEST_TYPE, TEST_DESCRIPTION)).thenReturn(Single.just(serverLoginReponse));

        createEventusecase.execute(TEST_LAT, TEST_LNG, TEST_TYPE, TEST_DESCRIPTION);

    }

}
