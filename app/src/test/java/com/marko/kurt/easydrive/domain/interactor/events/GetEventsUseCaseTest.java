package com.marko.kurt.easydrive.domain.interactor.events;

import com.marko.kurt.easydrive.domain.Event;
import com.marko.kurt.easydrive.domain.interactor.DefaultSingleUseCase;
import com.marko.kurt.easydrive.domain.repository.EventRepository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.subscribers.TestSubscriber;

/**
 * Created by kurt on 17/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class GetEventsUseCaseTest {

    private final Event TEST_EVENT = new Event();
    private final double TEST_LAT = 10.35;
    private final double TEST_LNG = 10.46;

    private GetEventsUseCase getEventsUseCase;
    private CompositeDisposable compositeDisposable;

    @Mock private EventRepository eventRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        getEventsUseCase = new GetEventsUseCase(compositeDisposable, eventRepository);
    }

    @Test
    public void shouldReturnAllEvents() throws Exception {

        final List<Event> events = new ArrayList<>();
        events.add(TEST_EVENT);

        Mockito.when(eventRepository.getEvents(TEST_LAT, TEST_LNG)).thenReturn(Single.just(events));

        getEventsUseCase.buildSingleUseCase(new double[]{TEST_LAT, TEST_LNG}).subscribeWith(new TestDisposableObserver<>());


        Mockito.verify(eventRepository, Mockito.times(1)).getEvents(TEST_LAT, TEST_LNG);
        Mockito.verifyNoMoreInteractions(eventRepository);

    }

    @Test
    public void testShouldFailWhenNoOrEmptyParameters() throws Exception {

        expectedException.expect(NullPointerException.class);
        getEventsUseCase.buildSingleUseCase(null);

    }

    private static class TestDisposableObserver<T> extends DisposableSingleObserver<T>{

        private int valueCOunt = 0;

        @Override
        public void onSuccess(T t) {
            valueCOunt++;
        }

        @Override
        public void onError(Throwable e) {

        }
    }

}
