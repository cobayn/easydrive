package com.marko.kurt.easydrive.data.repository;

import com.marko.kurt.easydrive.data.entity.CreateEventEntity;
import com.marko.kurt.easydrive.data.entity.EventEntity;
import com.marko.kurt.easydrive.data.entity.LikeEventEntity;
import com.marko.kurt.easydrive.data.entity.ServerLoginResponseEntity;
import com.marko.kurt.easydrive.data.entity.mapper.EventEntityDataMapper;
import com.marko.kurt.easydrive.data.net.EventApi;
import com.marko.kurt.easydrive.data.util.FileManager;
import com.marko.kurt.easydrive.data.util.TelephonIdGenerator;
import com.marko.kurt.easydrive.data.util.connectivity.ConnectivityManagerWrapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import retrofit2.Response;

import static org.mockito.BDDMockito.given;

/**
 * Created by user1 on 5.9.2017..
 */
@RunWith(MockitoJUnitRunner.class)
public class EventRepositoryImplTest {

    private static final int FAKE_EVENT_ID = 123;
    private static final short FAKE_GROUP_ID = 3;
    private static final short FAKE_RATE = 5;
    private static final String FAKE_DESCRIPTION = "cuje se skener";
    private static final double FAKE_LATITUDE = 40.3;
    private static final double FAKE_LONGITUDE = 50.4;

    private EventRepositoryImpl eventRepository;

    @Mock private EventApi eventApi;
    @Mock private FileManager fileManager;
    @Mock private ConnectivityManagerWrapper connectivityManagerWrapper;
    @Mock private TelephonIdGenerator telephonIdGenerator;

    @Before
    public void setUp() throws Exception {
        eventRepository = new EventRepositoryImpl(eventApi,
                fileManager,
                telephonIdGenerator,
                connectivityManagerWrapper, null);

    }

    @Test
    public void testGetEventsCase() throws Exception{
        Response<Object> eventList = Response.success(new Object());
        given(eventApi.getAllEvents(eventRepository.getQueryParams(5.1, 10.1), eventRepository.getHeaders())).willReturn(Single.just(eventList));

        eventRepository.getEvents(5.1,10.1);

        //verify(eventApi).getAllEvents(eventRepository.getQueryParams(5.1, 10.1), eventRepository.getHeaders());

    }

    @Test
    public void testLikeEventsCase() throws Exception {

        Response<ServerLoginResponseEntity> likeEvent = Response.success(new ServerLoginResponseEntity());

        given(eventApi.likeEvent(eventRepository.getHeaders(), new LikeEventEntity(FAKE_GROUP_ID, FAKE_RATE))).willReturn(Single.just(likeEvent));

    }

    @Test
    public void testDislikeEventsCase() throws Exception {

        Response<ServerLoginResponseEntity> likeEvent = Response.success(new ServerLoginResponseEntity());

        given(eventApi.dislikeEvent(eventRepository.getHeaders(), new LikeEventEntity(FAKE_GROUP_ID, FAKE_RATE))).willReturn(Single.just(likeEvent));

    }

    @Test
    public void createEventCase() throws Exception {

        Response<ServerLoginResponseEntity> likeEvent = Response.success(new ServerLoginResponseEntity());

        given(eventApi.createEvent(eventRepository.getHeaders(), new CreateEventEntity(FAKE_LATITUDE, FAKE_LONGITUDE, FAKE_RATE, FAKE_DESCRIPTION))).willReturn(Single.just(likeEvent));

    }

}
