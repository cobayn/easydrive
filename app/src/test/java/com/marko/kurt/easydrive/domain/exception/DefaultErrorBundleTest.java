package com.marko.kurt.easydrive.domain.exception;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * Created by user1 on 3.12.2017..
 */

@RunWith(MockitoJUnitRunner.class)
public class DefaultErrorBundleTest {

    private DefaultErrorBundle defaultErrorBundle;

    @Mock private Exception mockException;

    @Before
    public void setUp() throws Exception {
        defaultErrorBundle = new DefaultErrorBundle(mockException);
    }

    @Test
    public void testGetErrorMessageInteractor() {

        defaultErrorBundle.getErrorMessage();

        verify(mockException).getMessage();

    }
}
