package com.marko.kurt.easydrive.domain;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by nsoft on 06/09/2017.
 */

public class EventTest {

    private static final short FAKE_EVENT_ID = 5;

    private Event event;

    @Before
    public void setUp() {
        event = new Event();
        event.setId(FAKE_EVENT_ID);
    }

    @Test
    public void testEventConstructor() {
        final int eventId = event.getId();

        assertThat(event.getId(), is(FAKE_EVENT_ID));

    }



}
