package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.entity.GroupEventsEntity;
import com.marko.kurt.easydrive.domain.GroupEvents;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * Created by kurt on 17/11/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class GroupEventsEntityDataMapperTest {

    private static final short ID = 123;
    private static final String DESCRIPTION = "description";
    private static final short TYPE = 4;
    private static final double LAT = 43.43;
    private static final double LNG = 50.05;

    private GroupEventsEntityDataMapper groupEventMapper;

    @Before
    public void setUp() throws Exception {
        groupEventMapper = new GroupEventsEntityDataMapper();
    }

    @Test
    public void testTransformGroupEvents(){

        GroupEventsEntity groupEventEntity = createFakeEventEntity();
        GroupEvents groupEvents = groupEventMapper.transform(groupEventEntity);

        assertThat(groupEvents, is(instanceOf(GroupEvents.class)));
        assertThat(groupEvents.getId(), is(ID));
        assertThat(groupEvents.getDescription(), is(DESCRIPTION));
        assertThat(groupEvents.getEventType(), is(TYPE));
        assertThat(groupEvents.getLat(), is(LAT));
        assertThat(groupEvents.getLng(), is(LNG));

    }

    @Test
    public void testTransformGroupEventsList(){

        GroupEventsEntity mockGroupEntityOne = mock(GroupEventsEntity.class);
        GroupEventsEntity mockGroupEntityTwo = mock(GroupEventsEntity.class);

        List<GroupEventsEntity> groupEntities = new ArrayList<GroupEventsEntity>(5);
        groupEntities.add(mockGroupEntityOne);
        groupEntities.add(mockGroupEntityOne);

        Collection<GroupEvents> groupCollection = groupEventMapper.transformCollections(groupEntities);

        assertThat(groupCollection.toArray()[0], is(instanceOf(GroupEvents.class)));
        assertThat(groupCollection.toArray()[1], is(instanceOf(GroupEvents.class)));
        assertThat(groupCollection.size(), is(2));

    }

    private GroupEventsEntity createFakeEventEntity() {
        GroupEventsEntity groupEventsEntity = new GroupEventsEntity();
        groupEventsEntity.setId(ID);
        groupEventsEntity.setDescription(DESCRIPTION);
        groupEventsEntity.setEventType(TYPE);
        groupEventsEntity.setLat(LAT);
        groupEventsEntity.setLng(LNG);
        return groupEventsEntity;
    }

}
