package com.marko.kurt.easydrive.domain.interactor;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by kurt on 17/11/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultSingleUseCaseTest {

    private SingleUseCaseTestClass singleUseCase;
    private CompositeDisposable compositeDisposable;

    private TestDisposableObserver<Object> testDisposableObserver;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        this.singleUseCase = new SingleUseCaseTestClass(compositeDisposable);
        testDisposableObserver = new TestDisposableObserver();
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.io();
            }
        });
    }

    @Test
    public void testBuildUseCaseObservableReturnCorrectResult(){
        singleUseCase.execute(testDisposableObserver, null);
        singleUseCase.dispose();

    }

    @Test
    public void testShouldFailWhenExecuteWithNullObserver() {
        expectedException.expect(NullPointerException.class);
        singleUseCase.execute(null, null);
    }

    private static class SingleUseCaseTestClass extends DefaultSingleUseCase<Object, Void>{


        public SingleUseCaseTestClass(CompositeDisposable compositeDisposable) {
            super(compositeDisposable);
        }

        @Override
        public Single<Object> buildSingleUseCase(Void param) {
            return Single.create(emitter -> {
                emitter.onSuccess(new Object());
            });
        }

        @Override
        public void execute(DisposableSingleObserver<Object> observer, Void param) {
            super.execute(observer, param);
        }
    }

    private static class TestDisposableObserver<T> extends DisposableSingleObserver<T>{

        private int valueCOunt = 0;

        @Override
        public void onSuccess(T t) {
            valueCOunt++;
        }

        @Override
        public void onError(Throwable e) {

        }
    }

}
