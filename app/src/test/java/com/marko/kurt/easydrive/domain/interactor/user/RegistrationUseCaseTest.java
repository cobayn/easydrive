package com.marko.kurt.easydrive.domain.interactor.user;

import com.marko.kurt.easydrive.domain.ServerLoginReponse;
import com.marko.kurt.easydrive.domain.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by kurt on 17/11/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class RegistrationUseCaseTest {

    private static final String TEST_EMAIL = "kurt@gmail.com";
    private static final String TEST_USERNAME = "username";
    private static final String TEST_PASSWORD = "password";

    private RegistrationUserUseCase registrationUsecase;
    private CompositeDisposable compositeDisposable;
    @Mock private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        compositeDisposable = new CompositeDisposable();
        registrationUsecase = new RegistrationUserUseCase(compositeDisposable, userRepository);
    }

    @Test
    public void sucesfullyRegisterUser() throws Exception {

        ServerLoginReponse serverLoginReponse = new ServerLoginReponse();

        Mockito.when(userRepository.registerUser(TEST_EMAIL, TEST_USERNAME, TEST_PASSWORD)).thenReturn(Single.just(serverLoginReponse));

        registrationUsecase.buildSingleUseCase(new String[]{TEST_EMAIL, TEST_USERNAME, TEST_PASSWORD}).subscribeWith(new TestDisposableObserver<>());

        Mockito.verify(userRepository, Mockito.times(1)).registerUser(TEST_EMAIL, TEST_USERNAME, TEST_PASSWORD);
        Mockito.verifyNoMoreInteractions(userRepository);

    }

    private static class TestDisposableObserver<T> extends DisposableSingleObserver<T> {

        private int valueCOunt = 0;

        @Override
        public void onSuccess(T t) {
            valueCOunt++;
        }

        @Override
        public void onError(Throwable e) {

        }
    }

}
