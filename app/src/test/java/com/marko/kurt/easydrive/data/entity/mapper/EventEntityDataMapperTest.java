package com.marko.kurt.easydrive.data.entity.mapper;

import com.marko.kurt.easydrive.data.entity.EventEntity;
import com.marko.kurt.easydrive.data.entity.GroupEventsEntity;
import com.marko.kurt.easydrive.data.util.serializer.Serializer;
import com.marko.kurt.easydrive.domain.Event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Created by user1 on 5.9.2017..
 */
@RunWith(MockitoJUnitRunner.class)
public class EventEntityDataMapperTest {

    private static final short FAKE_EVENT_ID = 123;

    private EventEntityDataMapper eventEntityDataMapper;

    @Mock private EventDao eventDao;
    @Mock private Serializer mSerializer;
    private GroupEventsEntityDataMapper groupEventsEntityDataMapper;

    @Before
    public void setUp() throws Exception {
        groupEventsEntityDataMapper = new GroupEventsEntityDataMapper();
        eventEntityDataMapper = new EventEntityDataMapper(mSerializer, groupEventsEntityDataMapper, eventDao);
    }

    @Test
    public void testTransformEventEntity() {
        EventEntity eventEntity = createFakeEventEntity();
        Event event = eventEntityDataMapper.transform(eventEntity);

        assertThat(event, is(instanceOf(Event.class)));
        assertThat(event.getId(), is(FAKE_EVENT_ID));
    }

    @Test
    public void testTransformEventEntityCollection(){

        EventEntity mockEventEntityOne = mock(EventEntity.class);
        EventEntity mockEventEntityTwo = mock(EventEntity.class);

        List<EventEntity> eventEntities = new ArrayList<EventEntity>(5);
        eventEntities.add(mockEventEntityOne);
        eventEntities.add(mockEventEntityTwo);

        Collection<Event> eventCollection = eventEntityDataMapper.transform(eventEntities);

        assertThat(eventCollection.toArray()[0], is(instanceOf(Event.class)));
        assertThat(eventCollection.toArray()[1], is(instanceOf(Event.class)));
        assertThat(eventCollection.size(), is(2));

    }

    private EventEntity createFakeEventEntity() {
        EventEntity eventEntity = new EventEntity();
        eventEntity.setId(FAKE_EVENT_ID);
        eventEntity.setGroupEvents(createFakeGroupEventEntity());
        return eventEntity;
    }

    private List<GroupEventsEntity> createFakeGroupEventEntity(){
        List<GroupEventsEntity> listOfGroupEventsEntity = new ArrayList<>();
        GroupEventsEntity groupEventsEntity = new GroupEventsEntity();
        groupEventsEntity.setDescription("sta ima");
        groupEventsEntity.setLat(41.3456);
        groupEventsEntity.setLng(17.456);
        listOfGroupEventsEntity.add(groupEventsEntity);
        return listOfGroupEventsEntity;
    }

}
