package com.marko.kurt.easydrive.exception;

import android.test.AndroidTestCase;

import com.marko.kurt.easydrive.data.exception.NetworkConnectionException;
import com.marko.kurt.easydrive.presentation.exception.ErrorMessageFactory;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by user1 on 3.12.2017..
 */

public class ErrorMessageFactoryTest extends AndroidTestCase{

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testNetworkConnectionErrorMessage() {
        String errorMessage = "No internet connection";
        String actialMessage = ErrorMessageFactory.create(new NetworkConnectionException());

        assertThat(actialMessage, is(equalTo(errorMessage)));
    }

}
